package com.example.smartvillage;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.navigation.NavController;
import androidx.navigation.NavOptions;
import androidx.navigation.fragment.NavHostFragment;

import com.example.smartvillage.model.User;
import com.google.android.material.button.MaterialButton;

public class MineFragment extends Fragment {


    MainViewModel model;
    SharedPreferences sharedPref;

    TextView userName;

    MaterialButton btn_setting;


    public MineFragment() {
        super(R.layout.fragment_mine);
    }


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_mine, container, false);

        model = new ViewModelProvider(requireActivity()).get(MainViewModel.class);
        sharedPref = requireContext().getSharedPreferences(
                getString(R.string.preference_file_key), Context.MODE_PRIVATE);

        userName = view.findViewById(R.id.mine_user_name);

        userName.setText(model.getUser().getValue().getUserName());

        btn_setting = view.findViewById(R.id.setting);
        btn_setting.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(requireActivity(), SettingsActivity.class);
                MineFragment.super.requireContext().startActivity(intent);
            }
        });

        btn_setting = view.findViewById(R.id.album);
        btn_setting.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(requireActivity(), AlbumActivity.class);
                MineFragment.super.requireContext().startActivity(intent);
            }
        });

        btn_setting = view.findViewById(R.id.recommend);
        btn_setting.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new ShareFragment().show(getParentFragmentManager(),"Share");
            }
        });

        btn_setting = view.findViewById(R.id.favorite);
        btn_setting.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(requireActivity(), FavoriteActivity.class);
                MineFragment.super.requireContext().startActivity(intent);
            }
        });

        btn_setting = view.findViewById(R.id.logout);
        btn_setting.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
                builder.setTitle("是否退出登录")
                        .setMessage("请问确定要退出登录吗？")
                        .setPositiveButton("确定", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                sharedPref.edit()
                                        .putString("current_user_name", null)
                                        .putInt("current_user_identity", 0)
                                        .apply();
                                model.setUser(new User());

                                NavOptions navOptions = new NavOptions.Builder().setPopUpTo(R.id.homeFragment, true).build();
                                NavController navController = NavHostFragment.findNavController(requireParentFragment());
                                navController.navigate(R.id.action_mineFragment_to_identityFragment,null, navOptions);
                            }
                        })
                        .setNegativeButton("取消", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.cancel();
                            }
                        })
                        .show();
            }
        });

        return view;
    }

}