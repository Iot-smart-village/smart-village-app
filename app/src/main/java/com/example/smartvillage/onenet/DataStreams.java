package com.example.smartvillage.onenet;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

public class DataStreams {
    @SerializedName("datastreams")
    private List<DatastreamsDTO> datastreams;

    public DataStreams(String datastream_id,String value) {
        List<DatastreamsDTO> datastreams = new ArrayList<>();
        DatastreamsDTO datastreamsDTO = new DatastreamsDTO();
        datastreamsDTO.setId(datastream_id);
        datastreams.add(datastreamsDTO);
        DataStreams.DatastreamsDTO.DatapointsDTO datapointsDTO = new DataStreams.DatastreamsDTO.DatapointsDTO();
        datapointsDTO.setValue(value);
        datastreams.get(0).setDatapoints(new ArrayList<>());
        datastreams.get(0).getDatapoints().add(datapointsDTO);
        this.datastreams = datastreams;
    }

    public List<DatastreamsDTO> getDatastreams() {
        return datastreams;
    }

    public void setDatastreams(List<DatastreamsDTO> datastreams) {
        this.datastreams = datastreams;
    }

    public static class DatastreamsDTO {
        /**
         * id : temperature
         * datapoints : [{"at":"2013-04-22T00:35:43","value":"bacd"},{"at":"2013-04-22T00:55:43","value":84}]
         */

        @SerializedName("id")
        private String id;
        @SerializedName("datapoints")
        private List<DatapointsDTO> datapoints;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public List<DatapointsDTO> getDatapoints() {
            return datapoints;
        }

        public void setDatapoints(List<DatapointsDTO> datapoints) {
            this.datapoints = datapoints;
        }

        public static class DatapointsDTO {
            /**
             * at : 2013-04-22T00:35:43
             * value : bacd
             */
            @SerializedName("value")
            private String value;

            public String getValue() {
                return value;
            }

            public void setValue(String value) {
                this.value = value;
            }
        }
    }


}
