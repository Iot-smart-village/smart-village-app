package com.example.smartvillage.onenet;

import com.google.gson.JsonObject;

import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface OneNetApi {

    //基础URL： https://api.heclouds.com/

    String device_id = "693841590";

    //查询设备详情
    //
    //请求方式：GET
    //URL: https://api.heclouds.com/devices/device_id
    @GET("devices/"+device_id)
    Call<JsonObject> QueryDeviceDetail();


    //查询数据流详情
    //
    //请求方式：GET
    //URL: https://api.heclouds.com/devices/device_id/datastreams/datastream_id
    //
    //datastream_id ：需要替换为设备数据流ID
    @GET("devices/"+device_id+"/datastreams/{datastream_id}")
    Call<JsonObject> QueryDataStream(@Path("datastream_id") String datastream_id);


    //批量查询数据流信息
    //
    //请求方式：GET
    //
    //URL: https://api.heclouds.com/devices/device_id/datastreams
    //
    //datastream_ids:数据流ID，多个id之间用逗号分开，缺省时为查询所有数据流
    @GET("devices/"+device_id+"/datastreams")
    Call<JsonObject> BatchQueryDataStream(@Query("datastream_ids") String datastream_ids);


    //新增数据点
    //
    //请求方式：POST
    //
    //URL: https://api.heclouds.com/devices/device_id/datapoints
    //
    //device_id：需要替换为设备ID
    @POST("devices/"+device_id+"/datapoints")
    Call<JsonObject> UpdateDataStream(@Body() String dataStreams);


    //发送命令
    //
    //请求方式：POST
    //
    //URL:https://api.heclouds.com/cmds
    //
    //device_id:    接收该数据的设备ID
    //qos:          是否需要设备应答，默认为0。
    //              0：最多发送一次，不关心设备是否响应
    //              1：至少发送一次，如果设备收到命令后没有应答，则会在下一次设备登录时若命令在有效期内（有效期定义参见timeout参数）则会重发该命令
    //timeout:      命令有效时间，默认0。
    //              0：在线命令，若设备在线,下发给设备，若设备离线，直接丢弃
    //              >0： 离线命令，若设备在线，下发给设备，若设备离线，在当前时间加timeout时间内为有效期，有效期内，若设备上线，则下发给设备
    //              单位：秒
    //              有效范围：0~2678400
    @POST("cmds?device_id="+device_id)
    Call<JsonObject> SendCommand(@Query("qos") int qos,
                                 @Query("timeout") int timeout,
                                 @Body() RequestBody requestBody);
}
