package com.example.smartvillage;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AutoCompleteTextView;

import androidx.fragment.app.Fragment;

import com.google.android.material.appbar.MaterialToolbar;
import com.google.android.material.bottomnavigation.BottomNavigationView;


public class SearchFragment extends Fragment {


    MaterialToolbar toolbar;
    BottomNavigationView bottomNav;
    AutoCompleteTextView searchBar;


    public SearchFragment() {
        super(R.layout.fragment_search);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_search, container, false);

        toolbar = requireActivity().findViewById(R.id.toolbar);
        bottomNav = requireActivity().findViewById(R.id.bottom_nav);
        searchBar = requireActivity().findViewById(R.id.search_bar);

        //进入搜索界面时隐藏底部导航栏
        bottomNav.setVisibility(View.GONE);
        toolbar.findViewById(R.id.code_scan).setVisibility(View.GONE);

        return view;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        //离开搜索界面时显示底部导航栏
        bottomNav.setVisibility(View.VISIBLE);
        toolbar.findViewById(R.id.code_scan).setVisibility(View.VISIBLE);

        searchBar.clearFocus();
    }
}