package com.example.smartvillage.utils;

import androidx.recyclerview.widget.DiffUtil;

import com.example.smartvillage.model.NewsData;

import java.util.List;

public class NewsDiffCallback extends DiffUtil.Callback {
    private List<NewsData.ResultDTO.DataDTO> mOldData, mNewData;//看名字

    public NewsDiffCallback(List<NewsData.ResultDTO.DataDTO> mOldData, List<NewsData.ResultDTO.DataDTO> mNewData) {
        this.mOldData = mOldData;
        this.mNewData = mNewData;
    }

    @Override
    public int getOldListSize() {
        return mOldData != null ? mOldData.size() : 0;
    }

    @Override
    public int getNewListSize() {
        return mNewData != null ? mNewData.size() : 0;
    }

    @Override
    public boolean areItemsTheSame(int oldItemPosition, int newItemPosition) {
        return mOldData.get(oldItemPosition).getUniquekey().equals(mNewData.get(newItemPosition).getUniquekey());
    }

    @Override
    public boolean areContentsTheSame(int oldItemPosition, int newItemPosition) {
        NewsData.ResultDTO.DataDTO beanOld = mOldData.get(oldItemPosition);
        NewsData.ResultDTO.DataDTO beanNew = mNewData.get(newItemPosition);
        if (!beanOld.getTitle().equals(beanNew.getTitle())) {
            return false;//如果有内容不同，就返回false
        }
        if (!beanOld.getUrl().equals(beanNew.getUrl())) {
            return false;//如果有内容不同，就返回false
        }
        if (!beanOld.getThumbnailPicS().equals(beanNew.getThumbnailPicS())) {
            return false;//如果有内容不同，就返回false
        }
        if (!beanOld.getAuthorName().equals(beanNew.getAuthorName())) {
            return false;//如果有内容不同，就返回false
        }
        return beanOld.getDate().equals(beanNew.getDate());//如果有内容不同，就返回false
//默认两个data内容是相同的
    }
}
