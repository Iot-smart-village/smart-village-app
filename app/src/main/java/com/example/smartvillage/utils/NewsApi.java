package com.example.smartvillage.utils;

import androidx.annotation.Nullable;

import com.google.gson.JsonObject;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface NewsApi {

    String apiKey = "206fa8dfb578cb6b7e6262bdc7329f46";

    //接口地址：http://v.juhe.cn/toutiao/index
    //返回格式：json
    //请求方式：get/post
    //请求示例：http://v.juhe.cn/toutiao/index?type=top&key=APPKEY
    //接口备注：返回头条(推荐)、国内，娱乐，体育，军事，科技，财经，时尚等新闻信息; 数据来源网络整理;

    //  名称 	   必填 	类型 	    说明
    // 	key 	   是 	string 	    接口key, 在个人中心->我的数据,接口名称上方查看
    //  type 	   否 	string 	    支持类型
    //                              top(推荐,默认)
    //                              guonei(国内)
    //                              guoji(国际)
    //                              yule(娱乐)
    //                              tiyu(体育)
    //                              junshi(军事)
    //                              keji(科技)
    //                              caijing(财经)
    //                              shishang(时尚)
    //                              youxi(游戏)
    //                              qiche(汽车)
    //                              jiankang(健康)
    //  page 	    否 	 int 	    当前页数, 默认1, 最大50
    //  page_size 	否 	 int        每夜返回条数, 默认30 , 最大30
    @GET("index?key="+apiKey)
    Call<JsonObject> GetNewsList(@Nullable @Query("type") String type,
                                 @Query("page") int page,
                                 @Query("page_size") int page_size);

}
