package com.example.smartvillage;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.navigation.NavController;
import androidx.navigation.fragment.NavHostFragment;

import com.example.smartvillage.model.User;
import com.google.android.material.bottomnavigation.BottomNavigationView;

public class IdentityFragment extends Fragment {

    private static final String TAG = "登录页";

    MainViewModel model;
    NavController navController;

    Button btn_cunmin,btn_cunwei,btn_yisheng;
    BottomNavigationView bottomNav;

    public IdentityFragment() {
        super(R.layout.fragment_identity);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable final ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_identity, container, false);
        //取消全屏
        requireActivity().getWindow().clearFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN | WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS);
        //进入登陆界面时隐藏底部导航栏与应用栏
        bottomNav = requireActivity().findViewById(R.id.bottom_nav);
        bottomNav.setVisibility(View.GONE);
        ((AppCompatActivity)requireActivity()).getSupportActionBar().hide();

        model = new ViewModelProvider(requireActivity()).get(MainViewModel.class);
        navController = NavHostFragment.findNavController(requireParentFragment());

        btn_cunmin = view.findViewById(R.id.btn_cunmin);
        btn_cunwei = view.findViewById(R.id.btn_cunwei);
        btn_yisheng = view.findViewById(R.id.btn_yisheng);

        //身份码：0为无，1为村民，2为村委，3为医生
        btn_cunmin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                User user = model.getUser().getValue();
                user.setIdentity(1);
                model.setUser(user);
                navController.navigate(R.id.action_identityFragment_to_loginFragment);
            }
        });
        btn_cunwei.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                User user = model.getUser().getValue();
                user.setIdentity(2);
                model.setUser(user);
                navController.navigate(R.id.action_identityFragment_to_loginFragment);
            }
        });
        btn_yisheng.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                User user = model.getUser().getValue();
                user.setIdentity(3);
                model.setUser(user);
                navController.navigate(R.id.action_identityFragment_to_loginFragment);
            }
        });
        return view;
    }
}