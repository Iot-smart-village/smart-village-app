package com.example.smartvillage;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.navigation.NavController;
import androidx.navigation.NavOptions;
import androidx.navigation.fragment.NavHostFragment;

import com.example.smartvillage.model.User;
import com.google.android.material.bottomnavigation.BottomNavigationView;

public class LoginFragment extends Fragment {

    private static final String TAG = "登录页";

    MainViewModel model;
    SharedPreferences sharedPref;

    TextView tv_welcome;
    Button btn_login;
    EditText edit_user,edit_password;
    BottomNavigationView bottomNav;

    String Code_user = "sv";
    String Code_password = "123";

    public LoginFragment() {
        super(R.layout.fragment_login);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable final ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_login, container, false);
        requireActivity().getWindow().clearFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN | WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS);

        //进入登陆界面时隐藏底部导航栏与应用栏
        bottomNav = requireActivity().findViewById(R.id.bottom_nav);
        bottomNav.setVisibility(View.GONE);
        ((AppCompatActivity)requireActivity()).getSupportActionBar().hide();

        model = new ViewModelProvider(requireActivity()).get(MainViewModel.class);
        sharedPref = requireContext().getSharedPreferences(
                getString(R.string.preference_file_key), Context.MODE_PRIVATE);

        tv_welcome = view.findViewById(R.id.tv_welcome);
        btn_login = view.findViewById(R.id.btn_login);
        edit_user = view.findViewById(R.id.et_username);
        edit_password = view.findViewById(R.id.et_password);

        int identity_name_id = 0;
        switch (model.getUser().getValue().getIdentity()) {
            case 1:
                identity_name_id = R.string.cunmin;
                break;
            case 2:
                identity_name_id = R.string.cunwei;
                break;
            case 3:
                identity_name_id = R.string.yisheng;
                break;
        }
        tv_welcome.setText(Html.fromHtml(getString(R.string.welcome, getString(identity_name_id))));

        edit_user.setText(Code_user);
        edit_password.setText(Code_password);

        btn_login.setOnClickListener(v -> {
            String s_user = edit_user.getText().toString();

            if (s_user == null || s_user.length()==0){
                Toast.makeText(LoginFragment.super.getContext(),"请输入用户名",Toast.LENGTH_LONG).show();
            }

            else{
                String s_password = edit_password.getText().toString();
                if (s_password == null || s_password.length()==0){
                    Toast.makeText(LoginFragment.super.getContext(),"请输入密码",Toast.LENGTH_LONG).show();
                }
                else{
                    if(s_user.equals(Code_user)){
                        if(s_password.equals(Code_password)){
                            Toast.makeText(LoginFragment.super.getContext(),"登录成功",Toast.LENGTH_LONG).show();

                            //存储当前用户名
                            User currentUser = model.getUser().getValue();
                            currentUser.setUserName(s_user);
                            model.setUser(currentUser);

                            sharedPref.edit()
                                    .putString("current_user_name", currentUser.getUserName())
                                    .putInt("current_user_identity", currentUser.getIdentity())
                                    .apply();

                            NavOptions navOptions = new NavOptions.Builder().setPopUpTo(R.id.identityFragment, true).build();
                            NavController navController = NavHostFragment.findNavController(requireParentFragment());
                            navController.navigate(R.id.action_loginFragment_to_homeFragment,null, navOptions);

                        }else{
                            Toast.makeText(LoginFragment.super.getContext(),"登录失败",Toast.LENGTH_LONG).show();
                        }
                    }
                }
            }

        });
        return view;
    }
}