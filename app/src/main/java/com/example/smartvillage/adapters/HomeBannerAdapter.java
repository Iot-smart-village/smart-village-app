package com.example.smartvillage.adapters;

import com.example.smartvillage.R;
import com.example.smartvillage.model.BannerBean;
import com.zhpan.bannerview.BaseBannerAdapter;
import com.zhpan.bannerview.BaseViewHolder;

public class HomeBannerAdapter extends BaseBannerAdapter<BannerBean> {
    @Override
    protected void bindData(BaseViewHolder<BannerBean> holder, BannerBean data, int position, int pageSize) {
        holder.setImageResource(R.id.banner_image, data.getImgRes());
    }

    @Override
    public int getLayoutId(int viewType) {
        return R.layout.home_banner_item;
    }
}
