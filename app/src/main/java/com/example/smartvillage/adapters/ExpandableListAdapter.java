package com.example.smartvillage.adapters;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.TextView;

import androidx.appcompat.widget.AppCompatToggleButton;
import androidx.core.content.ContextCompat;

import com.example.smartvillage.R;

import java.util.HashMap;
import java.util.Map;

public class ExpandableListAdapter extends BaseExpandableListAdapter {

    private final Context context;
    private final String[] groupNames;
    private final String[][] childNames;
    private boolean[][] childValues;
    private ChildClickListener childClickListener;

    public interface ChildClickListener {
       void OnChildClick(View view, int groupPosition, int childPosition);
    }

    public void setChildClickListener(ChildClickListener childClickListener) {
        this.childClickListener = childClickListener;
    }

    @Override
    public View getGroupView(int groupPosition, boolean isExpanded, View convertView, ViewGroup parent) {

        class GroupHold {
            TextView  tvGroupName;
        }

        GroupHold groupHold;
        if (convertView == null) {
            convertView = LayoutInflater.from(context).inflate(R.layout.item_elv_group, parent, false);
            groupHold = new GroupHold();
            groupHold.tvGroupName = convertView.findViewById(R.id.tv_elv_groupName);

            convertView.setTag(groupHold);

        } else {
            groupHold = (GroupHold) convertView.getTag();
        }

        String groupName = groupNames[groupPosition];
        groupHold.tvGroupName.setText(groupName);
        //setTag() 方法接收的类型是object ，所以可将position和converView先封装在Map中。Bundle中无法封装view,所以不用bundle
        Map<String, Object> tagMap = new HashMap<>();
        tagMap.put("groupPosition", groupPosition);
        tagMap.put("isExpanded", isExpanded);

        return convertView;
    }

    @Override
    public View getChildView(int groupPosition, int childPosition, boolean isLastChild, View convertView, ViewGroup parent) {

        class ChildHold implements View.OnClickListener {
            AppCompatToggleButton childButton;
            final int groupPos = groupPosition;
            final int childPos = childPosition;

            public ChildHold(View view) {
                view.findViewById(R.id.button_elv_child).setOnClickListener(this);
            }

            @Override
            public void onClick(View v) {
                if (childClickListener != null)
                    childClickListener.OnChildClick(v, groupPos, childPos);
            }
        }

        ChildHold childHold;
        if (convertView == null) {
            convertView = LayoutInflater.from(context).inflate(R.layout.item_elv_child, parent, false);
            childHold = new ChildHold(convertView);
            childHold.childButton = convertView.findViewById(R.id.button_elv_child);
            convertView.setTag(childHold);
        } else {
            childHold = (ChildHold) convertView.getTag();
        }

        String childName = childNames[groupPosition][childPosition];
        boolean swState = childValues[groupPosition][childPosition];

        Drawable drawable;
        if (childName.contains("灯泡") || childName.contains("路灯")) {
            drawable = ContextCompat.getDrawable(context, R.drawable.ic_round_lightbulb_24);
        }
        else {
            drawable = ContextCompat.getDrawable(context, R.drawable.ic_round_sensor_window_24);
        }
        childHold.childButton.setTextOn(context.getString(R.string.something_on, childName));
        childHold.childButton.setTextOff(context.getString(R.string.something_off, childName));

        if (childHold.childButton.isChecked() != swState) {
            childHold.childButton.setChecked(swState);
        }

        if (childHold.childButton.isChecked()) {
            childHold.childButton.setText(context.getString(R.string.something_on, childName));
            childHold.childButton.setTextColor(context.getResources().getColor(R.color.white));
            drawable.setTint(context.getResources().getColor(R.color.white));
        }
        else {
            childHold.childButton.setText(context.getString(R.string.something_off, childName));
            childHold.childButton.setTextColor(context.getResources().getColor(R.color.darker_gray));
            drawable.setTint(context.getResources().getColor(R.color.darker_gray));
        }
        childHold.childButton.setCompoundDrawablesRelativeWithIntrinsicBounds(drawable,null,null,null);

        return convertView;
    }



    public ExpandableListAdapter(String[] groupNames, String[][] childNames, boolean[][] childValues, Context context) {
        this.groupNames = groupNames;
        this.childNames = childNames;
        this.childValues = childValues;
        this.context = context;
    }

    public void setChildValues(boolean[][] childValues) {
        this.childValues = childValues;
    }

    @Override
    public int getGroupCount() {
        return groupNames.length;
    }

    @Override
    public int getChildrenCount(int groupPosition) {
        return childNames[groupPosition].length;
    }

    @Override
    public Object getGroup(int groupPosition) {
        return groupNames[groupPosition];
    }

    @Override
    public Object getChild(int groupPosition, int childPosition) {
        return childNames[groupPosition][childPosition];
    }

    @Override
    public long getGroupId(int groupPosition) {
        return 0;
    }

    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return 0;
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }


    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return false;
    }
}
