package com.example.smartvillage.adapters;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.example.smartvillage.R;
import com.example.smartvillage.model.NewsData;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.List;

public class NewsListRecyclerAdapter extends RecyclerView.Adapter<NewsListRecyclerAdapter.ViewHolder> {

    private List<NewsData.ResultDTO.DataDTO> dataDTOs;
    private ItemClickListener itemClickListener;

    /**
     * Initialize the dataset of the Adapter.
     *
     * by RecyclerView.
     */
    public NewsListRecyclerAdapter() {
        this.dataDTOs = new ArrayList<>();
    }

    public List<NewsData.ResultDTO.DataDTO> getDataDTOs() {
        return dataDTOs;
    }

    public void setDataDTOs(List<NewsData.ResultDTO.DataDTO> dataDTOs) {
        this.dataDTOs = dataDTOs;
    }

    // allows clicks events to be caught
    public void setClickListener(ItemClickListener itemClickListener) {
        this.itemClickListener = itemClickListener;
    }

    // parent activity will implement this method to respond to click events
    public interface ItemClickListener {
        void onItemClick(View view, int position, String url);
    }

    /**
     * Provide a reference to the type of views that you are using
     * (custom ViewHolder).
     */
    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        private final TextView tv_news_title;
        private final TextView tv_news_author;
        private final TextView tv_news_date;
        private final ImageView tv_news_image;

        private String str_news_url;

        public ViewHolder(View view) {
            super(view);
            // Define click listener for the ViewHolder's View

            tv_news_title = view.findViewById(R.id.tv_news_title);
            tv_news_author = view.findViewById(R.id.tv_news_author);
            tv_news_date = view.findViewById(R.id.tv_news_date);
            tv_news_image = view.findViewById(R.id.iv_news_image);
            view.setOnClickListener(this);
        }

        public TextView getNewsTitle() {
            return tv_news_title;
        }

        public TextView getNewsAuthor() {
            return tv_news_author;
        }

        public TextView getNewsDate() {
            return tv_news_date;
        }

        public ImageView getNewsImage() {
            return tv_news_image;
        }

        public String getNewsUrl() {
            return str_news_url;
        }

        @Override
        public void onClick(View v) {
            if (itemClickListener != null)
                itemClickListener.onItemClick(v, getAdapterPosition(), getNewsUrl());
        }
    }

    // Create new views (invoked by the layout manager)
    @NotNull
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
        // Create a new view, which defines the UI of the list item
        View view = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.item_news_list, viewGroup, false);

        return new ViewHolder(view);
    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(ViewHolder viewHolder, final int position) {

        // Get element from your dataset at this position and replace the
        // contents of the view with that element
        viewHolder.getNewsTitle().setText(dataDTOs.get(position).getTitle());
        viewHolder.getNewsAuthor().setText(dataDTOs.get(position).getAuthorName());
        viewHolder.getNewsDate().setText(dataDTOs.get(position).getDate());
        viewHolder.getNewsImage().setClipToOutline(true);
        Glide.with(viewHolder.itemView.getContext()).load(dataDTOs.get(position).getThumbnailPicS()).into(viewHolder.getNewsImage());
        viewHolder.str_news_url = dataDTOs.get(position).getUrl();
    }

    // Return the size of your dataset (invoked by the layout manager)
    @Override
    public int getItemCount() {
        return dataDTOs.size();
    }

}
