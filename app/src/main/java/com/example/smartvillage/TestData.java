package com.example.smartvillage;

import com.example.smartvillage.model.ChatModel;
import com.example.smartvillage.model.ItemModel;

import java.util.ArrayList;

public class TestData {

    public static ArrayList<ItemModel> getTestAdData() {
        ArrayList<ItemModel> models = new ArrayList<>();
        ChatModel model = new ChatModel();
        model.setContent("您好？请问是哪里有感到不舒服吗？");
        model.setIcon("http://img.my.csdn.net/uploads/201508/05/1438760758_3497.jpg");
        models.add(new ItemModel(ItemModel.CHAT_A, model));
        ChatModel model2 = new ChatModel();
        model2.setContent("我最近感觉肠胃好像不太舒服");
        model2.setIcon("http://img.my.csdn.net/uploads/201508/05/1438760758_6667.jpg");
        models.add(new ItemModel(ItemModel.CHAT_B, model2));
        ChatModel model3 = new ChatModel();
        model3.setContent("那请详细说下病情吧");
        model3.setIcon("http://img.my.csdn.net/uploads/201508/05/1438760758_3497.jpg");
        models.add(new ItemModel(ItemModel.CHAT_A, model3));
        return models;
    }

}
