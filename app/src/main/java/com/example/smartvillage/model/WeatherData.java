package com.example.smartvillage.model;

import java.util.List;

public class WeatherData {

    /**
     * reason : 查询成功
     * result : {"city":"苏州","realtime":{"temperature":"4","humidity":"82","info":"阴","wid":"02","direct":"西北风","power":"3级","aqi":"80"},"future":[{"date":"2019-02-22","temperature":"1/7°C","weather":"小雨转多云","wid":{"day":"07","night":"01"},"direct":"北风转西北风"},{"date":"2019-02-23","temperature":"2/11°C","weather":"多云转阴","wid":{"day":"01","night":"02"},"direct":"北风转东北风"},{"date":"2019-02-24","temperature":"6/12°C","weather":"多云","wid":{"day":"01","night":"01"},"direct":"东北风转北风"},{"date":"2019-02-25","temperature":"5/12°C","weather":"小雨转多云","wid":{"day":"07","night":"01"},"direct":"东北风"},{"date":"2019-02-26","temperature":"5/11°C","weather":"多云转小雨","wid":{"day":"01","night":"07"},"direct":"东北风"}]}
     * error_code : 0
     */

    private String reason;
    private ResultDTO result;
    private int error_code;

    public String getReason() {
        return reason;
    }

    public ResultDTO getResult() {
        return result;
    }

    public int getError_code() {
        return error_code;
    }

    public static class ResultDTO {
        /**
         * city : 苏州
         * realtime : {"temperature":"4","humidity":"82","info":"阴","wid":"02","direct":"西北风","power":"3级","aqi":"80"}
         * future : [{"date":"2019-02-22","temperature":"1/7°C","weather":"小雨转多云","wid":{"day":"07","night":"01"},"direct":"北风转西北风"},{"date":"2019-02-23","temperature":"2/11°C","weather":"多云转阴","wid":{"day":"01","night":"02"},"direct":"北风转东北风"},{"date":"2019-02-24","temperature":"6/12°C","weather":"多云","wid":{"day":"01","night":"01"},"direct":"东北风转北风"},{"date":"2019-02-25","temperature":"5/12°C","weather":"小雨转多云","wid":{"day":"07","night":"01"},"direct":"东北风"},{"date":"2019-02-26","temperature":"5/11°C","weather":"多云转小雨","wid":{"day":"01","night":"07"},"direct":"东北风"}]
         */

        private String city;
        private RealtimeDTO realtime;
        private List<FutureDTO> future;

        public String getCity() {
            return city;
        }

        public RealtimeDTO getRealtime() {
            return realtime;
        }

        public List<FutureDTO> getFuture() {
            return future;
        }

        public static class RealtimeDTO {
            /**
             * temperature : 4
             * humidity : 82
             * info : 阴
             * wid : 02
             * direct : 西北风
             * power : 3级
             * aqi : 80
             */

            private String temperature;
            private String humidity;
            private String info;
            private String wid;
            private String direct;
            private String power;
            private String aqi;

            public String getTemperature() {
                return temperature;
            }

            public String getHumidity() {
                return humidity;
            }

            public String getInfo() {
                return info;
            }

            public String getWid() {
                return wid;
            }

            public String getDirect() {
                return direct;
            }

            public String getPower() {
                return power;
            }

            public String getAqi() {
                return aqi;
            }
        }

        public static class FutureDTO {
            /**
             * date : 2019-02-22
             * temperature : 1/7°C
             * weather : 小雨转多云
             * wid : {"day":"07","night":"01"}
             * direct : 北风转西北风
             */

            private String date;
            private String temperature;
            private String weather;
            private WidDTO wid;
            private String direct;

            public String getDate() {
                return date;
            }

            public String getTemperature() {
                return temperature;
            }

            public String getWeather() {
                return weather;
            }

            public WidDTO getWid() {
                return wid;
            }

            public String getDirect() {
                return direct;
            }

            public static class WidDTO {
                /**
                 * day : 07
                 * night : 01
                 */

                private String day;
                private String night;

                public String getDay() {
                    return day;
                }

                public String getNight() {
                    return night;
                }
            }
        }
    }
}
