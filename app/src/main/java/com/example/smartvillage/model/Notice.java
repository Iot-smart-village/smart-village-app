package com.example.smartvillage.model;

import cn.bmob.v3.BmobObject;

public class Notice extends BmobObject {
    private String notice;
    public String getNotice(){return notice;}
    public void setNotice(String notice){this.notice = notice;}
    @Override
    public String toString(){return notice;}
}
