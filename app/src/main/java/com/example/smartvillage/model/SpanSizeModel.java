package com.example.smartvillage.model;

public interface SpanSizeModel {

    int getSpanSize();

    void setSpanSize(int spanSize);

    long getTimestamp();

}
