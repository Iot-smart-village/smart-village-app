package com.example.smartvillage.model;

import java.util.List;

public interface ModelProvider {

    List<? extends SpanSizeModel> getModels();

}
