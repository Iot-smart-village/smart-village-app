package com.example.smartvillage.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class NewsData {
    /**
     * reason : success!
     * result : {"stat":"1","data":[{"uniquekey":"b786faff07c51ecd019d83149a2277e6","title":"喜讯！正荣地产荣获\u201c绿色金融发行后认证证书\u201d","date":"2021-03-26 14:29:00","category":"头条","author_name":"\u201c娱\u201d我同行","url":"https://mini.eastday.com/mobile/210326142911140295956.html","thumbnail_pic_s":"https://dfzximg02.dftoutiao.com/news/20210326/20210326142911_bb323782361f4205144e7b6d7090c50f_1_mwpm_03201609.png","thumbnail_pic_s02":"https://dfzximg02.dftoutiao.com/news/20210326/20210326142911_bb323782361f4205144e7b6d7090c50f_2_mwpm_03201609.png","thumbnail_pic_s03":"https://dfzximg02.dftoutiao.com/news/20210326/20210326142911_bb323782361f4205144e7b6d7090c50f_3_mwpm_03201609.png","is_content":"1"},{"uniquekey":"83d9c2d7afb2f8f6336d107992f8b26c","title":"悄然而至的锦旗","date":"2021-03-26 14:28:00","category":"头条","author_name":"中国网","url":"https://mini.eastday.com/mobile/210326142851423113739.html","thumbnail_pic_s":"https://dfzximg02.dftoutiao.com/news/20210326/20210326142851_2d43b343d79889b97e45b9888f049cf6_1_mwpm_03201609.jpeg","is_content":"1"},{"uniquekey":"368499cdd6cc6d3bbc6bca684e9dcc3b","title":"缙云：书法名家进校园 墨韵飘香润童心","date":"2021-03-26 14:28:00","category":"头条","author_name":"浙江在线-缙云新闻网","url":"https://mini.eastday.com/mobile/210326142820818273323.html","thumbnail_pic_s":"https://dfzximg02.dftoutiao.com/news/20210326/20210326142820_6b7c67f92bf98cb9654d94569ef17e05_1_mwpm_03201609.jpeg","thumbnail_pic_s02":"https://dfzximg02.dftoutiao.com/news/20210326/20210326142820_6b7c67f92bf98cb9654d94569ef17e05_2_mwpm_03201609.jpeg","thumbnail_pic_s03":"https://dfzximg02.dftoutiao.com/news/20210326/20210326142820_6b7c67f92bf98cb9654d94569ef17e05_3_mwpm_03201609.jpeg","is_content":"1"},{"uniquekey":"d0287d984d0ef4c1938ddb192014a260","title":"石楼：两辆电动车追尾 一人重伤","date":"2021-03-26 14:24:00","category":"头条","author_name":"黄河新闻网吕梁频道","url":"https://mini.eastday.com/mobile/210326142427193862944.html","thumbnail_pic_s":"https://dfzximg02.dftoutiao.com/news/20210326/20210326142427_bfe420db5a131d0a14af6613ad628360_1_mwpm_03201609.jpeg","is_content":"1"},{"uniquekey":"359ae35fce2b97a99821aa009a765a31","title":"约见网友被诈骗 民警追赃终返还","date":"2021-03-26 14:18:00","category":"头条","author_name":"江南时报","url":"https://mini.eastday.com/mobile/210326141819966329651.html","thumbnail_pic_s":"https://dfzximg02.dftoutiao.com/news/20210326/20210326141819_4cdb0ed28fce0bafe5a65e6a89911422_1_mwpm_03201609.png","thumbnail_pic_s02":"https://dfzximg02.dftoutiao.com/news/20210326/20210326141819_4cdb0ed28fce0bafe5a65e6a89911422_2_mwpm_03201609.png","is_content":"1"},{"uniquekey":"94d050b508c42256c6cc38394e0089f5","title":"汇聚金融力量 践行绿色发展\u2014\u2014平安租赁荣获\"陆家嘴金融城绿色金融综合发展平台绿色项目\"认证","date":"2021-03-26 14:17:00","category":"头条","author_name":"消费日报网","url":"https://mini.eastday.com/mobile/210326141728276621281.html","thumbnail_pic_s":"https://dfzximg02.dftoutiao.com/news/20210326/20210326141728_d41d8cd98f00b204e9800998ecf8427e_1_mwpm_03201609.png","is_content":"1"},{"uniquekey":"0249274a04402a9c0787c6438263ea3f","title":"响应中新自贸协定升级，鑫荣懋集团与佳沛公司共庆佳沛新西兰奇异果上市","date":"2021-03-26 14:15:00","category":"头条","author_name":"消费日报网","url":"https://mini.eastday.com/mobile/210326141522769889900.html","thumbnail_pic_s":"https://dfzximg02.dftoutiao.com/news/20210326/20210326141522_d41d8cd98f00b204e9800998ecf8427e_1_mwpm_03201609.png","thumbnail_pic_s02":"https://dfzximg02.dftoutiao.com/news/20210326/20210326141522_d41d8cd98f00b204e9800998ecf8427e_2_mwpm_03201609.png","thumbnail_pic_s03":"https://dfzximg02.dftoutiao.com/news/20210326/20210326141522_d41d8cd98f00b204e9800998ecf8427e_3_mwpm_03201609.png","is_content":"1"},{"uniquekey":"5a6e52bb2acf56ea311bbd9ae176f7b3","title":"\u201c医术精湛高超\u2022创造生命奇迹\u201d\u2014\u2014一面来自患者的特殊锦旗","date":"2021-03-26 14:13:00","category":"头条","author_name":"消费日报网","url":"https://mini.eastday.com/mobile/210326141312479880867.html","thumbnail_pic_s":"https://dfzximg02.dftoutiao.com/news/20210326/20210326141312_d41d8cd98f00b204e9800998ecf8427e_1_mwpm_03201609.png","thumbnail_pic_s02":"https://dfzximg02.dftoutiao.com/news/20210326/20210326141312_d41d8cd98f00b204e9800998ecf8427e_2_mwpm_03201609.png","thumbnail_pic_s03":"https://dfzximg02.dftoutiao.com/news/20210326/20210326141312_d41d8cd98f00b204e9800998ecf8427e_3_mwpm_03201609.png","is_content":"1"},{"uniquekey":"9515c5d879e5fea501595e86dae6b796","title":"不放弃拼到底！今晚，上海男排放手一搏","date":"2021-03-26 14:08:00","category":"头条","author_name":"新民晚报","url":"https://mini.eastday.com/mobile/210326140800856667202.html","thumbnail_pic_s":"https://dfzximg02.dftoutiao.com/news/20210326/20210326140800_7b70a1ce85f6df4904506abb24d4a864_1_mwpm_03201609.jpeg","thumbnail_pic_s02":"https://dfzximg02.dftoutiao.com/news/20210326/20210326140800_7b70a1ce85f6df4904506abb24d4a864_2_mwpm_03201609.jpeg","thumbnail_pic_s03":"https://dfzximg02.dftoutiao.com/news/20210326/20210326140800_7b70a1ce85f6df4904506abb24d4a864_3_mwpm_03201609.jpeg","is_content":"1"},{"uniquekey":"69b4b7c2bba13f698729d15953d62cb1","title":"\u201c十三邀\u201d中青年名家书画交流展在合川美术馆开幕","date":"2021-03-26 14:04:00","category":"头条","author_name":"徐之腾艺术空间","url":"https://mini.eastday.com/mobile/210326140422713749616.html","thumbnail_pic_s":"https://dfzximg02.dftoutiao.com/news/20210326/20210326140422_cb454e4c6e40d9c05c1c0f33238577d3_1_mwpm_03201609.jpeg","thumbnail_pic_s02":"https://dfzximg02.dftoutiao.com/news/20210326/20210326140422_cb454e4c6e40d9c05c1c0f33238577d3_2_mwpm_03201609.jpeg","thumbnail_pic_s03":"https://dfzximg02.dftoutiao.com/news/20210326/20210326140422_cb454e4c6e40d9c05c1c0f33238577d3_3_mwpm_03201609.jpeg","is_content":"1"},{"uniquekey":"a017ea93e3666f745f7b7002d53bee58","title":"女子车祸身亡，留下2590万元天价保单，警方发现事情不简单","date":"2021-03-26 14:03:00","category":"头条","author_name":"新快报","url":"https://mini.eastday.com/mobile/210326140342949732225.html","thumbnail_pic_s":"https://dfzximg02.dftoutiao.com/news/20210326/20210326140342_80aa3bb5dd301480dfb477f76da30c64_1_mwpm_03201609.jpeg","is_content":"1"},{"uniquekey":"76453a71458ab6b89bb418492acb0d3b","title":"多名主播带货遭网友围攻 耐克小姐姐被骂哭","date":"2021-03-26 14:03:00","category":"头条","author_name":"大象新闻","url":"https://mini.eastday.com/mobile/210326140324528810878.html","thumbnail_pic_s":"https://dfzximg02.dftoutiao.com/news/20210326/20210326140324_dee64f99b29ca4cdc98a72a5361d01d3_0_mwpm_03201609.jpeg","thumbnail_pic_s02":"https://dfzximg02.dftoutiao.com/news/20210326/20210326140324_dee64f99b29ca4cdc98a72a5361d01d3_1_mwpm_03201609.jpeg","thumbnail_pic_s03":"https://dfzximg02.dftoutiao.com/news/20210326/20210326140324_dee64f99b29ca4cdc98a72a5361d01d3_2_mwpm_03201609.jpeg","is_content":"1"},{"uniquekey":"59c90f520a13035ec8ff46d393bc16b8","title":"40年喝了过10000斤白酒 男子长出\u201c象鼻子\u201d","date":"2021-03-26 14:02:00","category":"头条","author_name":"辽宁台新闻中心","url":"https://mini.eastday.com/mobile/210326140246161635523.html","thumbnail_pic_s":"https://dfzximg02.dftoutiao.com/news/20210326/20210326140246_9d87536cbff07bd21af109666b947413_1_mwpm_03201609.png","thumbnail_pic_s02":"https://dfzximg02.dftoutiao.com/news/20210326/20210326140246_9d87536cbff07bd21af109666b947413_2_mwpm_03201609.png","is_content":"1"},{"uniquekey":"83566c5419054e13f8deb1874102b67d","title":"南宁哈罗礼德奖学金计划公布 携手剑桥大学\u2014\u2014哈罗优秀校友分享国际化成长之路","date":"2021-03-26 13:57:00","category":"头条","author_name":"中国日报网","url":"https://mini.eastday.com/mobile/210326135701888416387.html","thumbnail_pic_s":"https://dfzximg02.dftoutiao.com/news/20210326/20210326135701_b9343f20d00256e57a339742a7fb898a_0_mwpm_03201609.jpeg","thumbnail_pic_s02":"https://dfzximg02.dftoutiao.com/news/20210326/20210326135701_b9343f20d00256e57a339742a7fb898a_1_mwpm_03201609.jpeg","thumbnail_pic_s03":"https://dfzximg02.dftoutiao.com/news/20210326/20210326135701_b9343f20d00256e57a339742a7fb898a_2_mwpm_03201609.jpeg","is_content":"1"},{"uniquekey":"4e57453a7e0b19606fda3b104b3dbd0b","title":"3月28日卤人甲加盟费限时下调，热门小吃三个月回本","date":"2021-03-26 13:57:00","category":"头条","author_name":"经济网","url":"https://mini.eastday.com/mobile/210326135701117323100.html","thumbnail_pic_s":"https://dfzximg02.dftoutiao.com/news/20210326/20210326135701_cc67e280f3fe771e5caf330cd000cb34_0_mwpm_03201609.jpeg","thumbnail_pic_s02":"https://dfzximg02.dftoutiao.com/news/20210326/20210326135701_cc67e280f3fe771e5caf330cd000cb34_1_mwpm_03201609.png","thumbnail_pic_s03":"https://dfzximg02.dftoutiao.com/news/20210326/20210326135701_cc67e280f3fe771e5caf330cd000cb34_2_mwpm_03201609.png","is_content":"1"},{"uniquekey":"8d24bef810ed97cc3410adc68633d6cf","title":"出口易完成亚马逊MFN轨迹对接，助力卖家轻松发运！","date":"2021-03-26 13:57:00","category":"头条","author_name":"经济网","url":"https://mini.eastday.com/mobile/210326135700134473326.html","thumbnail_pic_s":"https://dfzximg02.dftoutiao.com/news/20210326/20210326135700_6e3ac02a041b26da68c1277513edc0d2_1_mwpm_03201609.png","thumbnail_pic_s02":"https://dfzximg02.dftoutiao.com/news/20210326/20210326135700_6e3ac02a041b26da68c1277513edc0d2_2_mwpm_03201609.jpeg","thumbnail_pic_s03":"https://dfzximg02.dftoutiao.com/news/20210326/20210326135700_6e3ac02a041b26da68c1277513edc0d2_3_mwpm_03201609.jpeg","is_content":"1"},{"uniquekey":"dbce57253494673b229155a94c58a3e5","title":"疫情逆势增长，土豆教育重磅发布全能课程体系3.0","date":"2021-03-26 13:56:00","category":"头条","author_name":"国际在线","url":"https://mini.eastday.com/mobile/210326135659569749375.html","thumbnail_pic_s":"https://dfzximg02.dftoutiao.com/news/20210326/20210326135659_264e08d1c86ea5f239636353af860b94_0_mwpm_03201609.png","thumbnail_pic_s02":"https://dfzximg02.dftoutiao.com/news/20210326/20210326135659_264e08d1c86ea5f239636353af860b94_1_mwpm_03201609.jpeg","thumbnail_pic_s03":"https://dfzximg02.dftoutiao.com/news/20210326/20210326135659_264e08d1c86ea5f239636353af860b94_2_mwpm_03201609.png","is_content":"1"},{"uniquekey":"6f2908ce92d06292144c90a2b528da13","title":"美图2020年财报：营收大增22.1%，收入结构更加健康","date":"2021-03-26 13:56:00","category":"头条","author_name":"凤凰网","url":"https://mini.eastday.com/mobile/210326135658883182782.html","thumbnail_pic_s":"https://dfzximg02.dftoutiao.com/news/20210326/20210326135658_6cb4692239b37f119d03a3c3abef5b5e_0_mwpm_03201609.jpg","is_content":"1"},{"uniquekey":"2565f127eeb40c6a55ee3ad0237499a2","title":"人瑞人才（6919.HK）荣获金帜奖\u201c2021最佳人力资源综合服务供应商\u201d","date":"2021-03-26 13:56:00","category":"头条","author_name":"中国金融网","url":"https://mini.eastday.com/mobile/210326135657822598871.html","thumbnail_pic_s":"https://dfzximg02.dftoutiao.com/news/20210326/20210326135657_82dc4f0a4702ac5b366b1027cbb329b8_0_mwpm_03201609.jpeg","is_content":"1"},{"uniquekey":"62db77d8ec0b3f83c7a4fa6cb991d17c","title":"打破常规才能创新突破，润初妍益生菌水乳即将上市！","date":"2021-03-26 13:56:00","category":"头条","author_name":"中国资讯网","url":"https://mini.eastday.com/mobile/210326135657370709287.html","thumbnail_pic_s":"https://dfzximg02.dftoutiao.com/news/20210326/20210326135657_91bc6f24111249551b195f233949543e_0_mwpm_03201609.png","thumbnail_pic_s02":"https://dfzximg02.dftoutiao.com/news/20210326/20210326135657_91bc6f24111249551b195f233949543e_1_mwpm_03201609.png","thumbnail_pic_s03":"https://dfzximg02.dftoutiao.com/news/20210326/20210326135657_91bc6f24111249551b195f233949543e_2_mwpm_03201609.png","is_content":"1"},{"uniquekey":"d49d4a807733605f433ae248ef310730","title":"健康证下来了，我和股市都是健康的","date":"2021-03-26 13:53:00","category":"头条","author_name":"年糕猫","url":"https://mini.eastday.com/mobile/210326135344666237388.html","thumbnail_pic_s":"https://dfzximg02.dftoutiao.com/news/20210326/20210326135344_96db3334b24411f9837c127471acdcd9_1_mwpm_03201609.jpeg","is_content":"1"},{"uniquekey":"4fb08033935000d9a57bab1a22311c0f","title":"\u201c无\u201d中 生\u201c有\u201d\u2014\u2014马路炁象作品展将于西安崔振宽美术馆开展","date":"2021-03-26 13:48:00","category":"头条","author_name":"国画家","url":"https://mini.eastday.com/mobile/210326134851551994895.html","thumbnail_pic_s":"https://dfzximg02.dftoutiao.com/news/20210326/20210326134851_219b2414e87176d44e9cef071d6e2095_1_mwpm_03201609.jpeg","thumbnail_pic_s02":"https://dfzximg02.dftoutiao.com/news/20210326/20210326134851_219b2414e87176d44e9cef071d6e2095_2_mwpm_03201609.jpeg","thumbnail_pic_s03":"https://dfzximg02.dftoutiao.com/news/20210326/20210326134851_219b2414e87176d44e9cef071d6e2095_3_mwpm_03201609.jpeg","is_content":"1"},{"uniquekey":"573145e5f6de7aecdb761b66253b7efa","title":"\u201c头条寻人\u201d助走失30年女子回家，并捐助万元济困","date":"2021-03-26 13:48:00","category":"头条","author_name":"消费日报网","url":"https://mini.eastday.com/mobile/210326134810605851754.html","thumbnail_pic_s":"https://dfzximg02.dftoutiao.com/news/20210326/20210326134810_d41d8cd98f00b204e9800998ecf8427e_1_mwpm_03201609.png","is_content":"1"},{"uniquekey":"a1fc48c2a2822b486e540d6ddfb1c013","title":"「枫警故事」老人报案丢失财物 民警相助\u201c失\u201d而复得","date":"2021-03-26 13:47:00","category":"头条","author_name":"消费日报网","url":"https://mini.eastday.com/mobile/210326134734976562479.html","thumbnail_pic_s":"https://dfzximg02.dftoutiao.com/news/20210326/20210326134734_d41d8cd98f00b204e9800998ecf8427e_1_mwpm_03201609.png","thumbnail_pic_s02":"https://dfzximg02.dftoutiao.com/news/20210326/20210326134734_d41d8cd98f00b204e9800998ecf8427e_2_mwpm_03201609.png","thumbnail_pic_s03":"https://dfzximg02.dftoutiao.com/news/20210326/20210326134734_d41d8cd98f00b204e9800998ecf8427e_3_mwpm_03201609.png","is_content":"1"},{"uniquekey":"94221b2f65bc49cd005a3936d99411d6","title":"基站成\u201c邻居\u201d太吵了 铁塔公司:物业收高额场租","date":"2021-03-26 13:47:00","category":"头条","author_name":"看看新闻Knews","url":"https://mini.eastday.com/mobile/210326134708087424846.html","thumbnail_pic_s":"https://dfzximg02.dftoutiao.com/news/20210326/20210326134708_54af9f94fc7c79c25a7b59ea2a64a46c_1_mwpm_03201609.png","thumbnail_pic_s02":"https://dfzximg02.dftoutiao.com/news/20210326/20210326134708_54af9f94fc7c79c25a7b59ea2a64a46c_2_mwpm_03201609.png","thumbnail_pic_s03":"https://dfzximg02.dftoutiao.com/news/20210326/20210326134708_54af9f94fc7c79c25a7b59ea2a64a46c_3_mwpm_03201609.png","is_content":"1"},{"uniquekey":"597ad4b782b6c965a0165d2903b66718","title":"\u201c梅姨案\u201d受害者申军良索赔480万，儿子仍在适应新生活","date":"2021-03-26 13:43:00","category":"头条","author_name":"中国新闻周刊","url":"https://mini.eastday.com/mobile/210326134308036311736.html","thumbnail_pic_s":"https://dfzximg02.dftoutiao.com/news/20210326/20210326134308_46dadcbf0940b5f65b4422abc430451f_1_mwpm_03201609.jpeg","thumbnail_pic_s02":"https://dfzximg02.dftoutiao.com/news/20210326/20210326134308_46dadcbf0940b5f65b4422abc430451f_2_mwpm_03201609.jpeg","thumbnail_pic_s03":"https://dfzximg02.dftoutiao.com/news/20210326/20210326134308_46dadcbf0940b5f65b4422abc430451f_3_mwpm_03201609.jpeg","is_content":"1"},{"uniquekey":"2da6d4b1b906e3fd7e79be3b91614627","title":"跨界营销，COLMO洗碗机净洗之夜如何借\u201c脱口秀+红酒品鉴\u201d实现突围？","date":"2021-03-26 13:41:00","category":"头条","author_name":"中国网","url":"https://mini.eastday.com/mobile/210326134153209213476.html","thumbnail_pic_s":"https://dfzximg02.dftoutiao.com/news/20210326/20210326134153_6a34f7753263fdfc0480612aa3d69e6a_0_mwpm_03201609.jpeg","thumbnail_pic_s02":"https://dfzximg02.dftoutiao.com/news/20210326/20210326134153_6a34f7753263fdfc0480612aa3d69e6a_1_mwpm_03201609.jpeg","thumbnail_pic_s03":"https://dfzximg02.dftoutiao.com/news/20210326/20210326134153_6a34f7753263fdfc0480612aa3d69e6a_2_mwpm_03201609.jpeg","is_content":"1"},{"uniquekey":"d65707fd8735ff6b6670b84ab51ae287","title":"通过性传播的4种性病，第一种最可怕！思想开放的人要小心了","date":"2021-03-26 13:39:00","category":"头条","author_name":"家庭医生名医在线","url":"https://mini.eastday.com/mobile/210326133930767621320.html","thumbnail_pic_s":"https://dfzximg02.dftoutiao.com/news/20210326/20210326133930_7ba7a4c122fb41c1d6885e96464f7dfe_1_mwpm_03201609.jpeg","thumbnail_pic_s02":"https://dfzximg02.dftoutiao.com/news/20210326/20210326133930_7ba7a4c122fb41c1d6885e96464f7dfe_2_mwpm_03201609.jpeg","thumbnail_pic_s03":"https://dfzximg02.dftoutiao.com/news/20210326/20210326133930_7ba7a4c122fb41c1d6885e96464f7dfe_3_mwpm_03201609.jpeg","is_content":"1"},{"uniquekey":"12eda6e206a992b3af9065d9ba54633e","title":"张维平拐卖儿童案二审开庭，受害家属：不希望人贩子被立即执行死刑","date":"2021-03-26 13:37:00","category":"头条","author_name":"新快报","url":"https://mini.eastday.com/mobile/210326133726833226433.html","thumbnail_pic_s":"https://dfzximg02.dftoutiao.com/news/20210326/20210326133726_c1e58ade89a0384c00184de2cfc04d2b_1_mwpm_03201609.jpeg","thumbnail_pic_s02":"https://dfzximg02.dftoutiao.com/news/20210326/20210326133726_c1e58ade89a0384c00184de2cfc04d2b_2_mwpm_03201609.jpeg","thumbnail_pic_s03":"https://dfzximg02.dftoutiao.com/news/20210326/20210326133726_c1e58ade89a0384c00184de2cfc04d2b_3_mwpm_03201609.jpeg","is_content":"1"},{"uniquekey":"f4670394cdc24a291ab2caa936168d66","title":"第六届浙江大学创业大赛北部赛区决赛暨颁奖典礼在首科大厦成功举办","date":"2021-03-26 13:28:00","category":"头条","author_name":"丰台组工","url":"https://mini.eastday.com/mobile/210326132847650623979.html","thumbnail_pic_s":"https://dfzximg02.dftoutiao.com/news/20210326/20210326132847_23088687eae25f8899f8914a834972d1_0_mwpm_03201609.jpeg","thumbnail_pic_s02":"https://dfzximg02.dftoutiao.com/news/20210326/20210326132847_23088687eae25f8899f8914a834972d1_1_mwpm_03201609.jpeg","thumbnail_pic_s03":"https://dfzximg02.dftoutiao.com/news/20210326/20210326132847_23088687eae25f8899f8914a834972d1_2_mwpm_03201609.jpeg","is_content":"1"}],"page":"1","pageSize":"30"}
     * error_code : 0
     */

    @SerializedName("reason")
    private String reason;
    @SerializedName("result")
    private ResultDTO result;
    @SerializedName("error_code")
    private Integer errorCode;

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }

    public ResultDTO getResult() {
        return result;
    }

    public void setResult(ResultDTO result) {
        this.result = result;
    }

    public Integer getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(Integer errorCode) {
        this.errorCode = errorCode;
    }

    public static class ResultDTO {
        /**
         * stat : 1
         * data : [{"uniquekey":"b786faff07c51ecd019d83149a2277e6","title":"喜讯！正荣地产荣获\u201c绿色金融发行后认证证书\u201d","date":"2021-03-26 14:29:00","category":"头条","author_name":"\u201c娱\u201d我同行","url":"https://mini.eastday.com/mobile/210326142911140295956.html","thumbnail_pic_s":"https://dfzximg02.dftoutiao.com/news/20210326/20210326142911_bb323782361f4205144e7b6d7090c50f_1_mwpm_03201609.png","thumbnail_pic_s02":"https://dfzximg02.dftoutiao.com/news/20210326/20210326142911_bb323782361f4205144e7b6d7090c50f_2_mwpm_03201609.png","thumbnail_pic_s03":"https://dfzximg02.dftoutiao.com/news/20210326/20210326142911_bb323782361f4205144e7b6d7090c50f_3_mwpm_03201609.png","is_content":"1"},{"uniquekey":"83d9c2d7afb2f8f6336d107992f8b26c","title":"悄然而至的锦旗","date":"2021-03-26 14:28:00","category":"头条","author_name":"中国网","url":"https://mini.eastday.com/mobile/210326142851423113739.html","thumbnail_pic_s":"https://dfzximg02.dftoutiao.com/news/20210326/20210326142851_2d43b343d79889b97e45b9888f049cf6_1_mwpm_03201609.jpeg","is_content":"1"},{"uniquekey":"368499cdd6cc6d3bbc6bca684e9dcc3b","title":"缙云：书法名家进校园 墨韵飘香润童心","date":"2021-03-26 14:28:00","category":"头条","author_name":"浙江在线-缙云新闻网","url":"https://mini.eastday.com/mobile/210326142820818273323.html","thumbnail_pic_s":"https://dfzximg02.dftoutiao.com/news/20210326/20210326142820_6b7c67f92bf98cb9654d94569ef17e05_1_mwpm_03201609.jpeg","thumbnail_pic_s02":"https://dfzximg02.dftoutiao.com/news/20210326/20210326142820_6b7c67f92bf98cb9654d94569ef17e05_2_mwpm_03201609.jpeg","thumbnail_pic_s03":"https://dfzximg02.dftoutiao.com/news/20210326/20210326142820_6b7c67f92bf98cb9654d94569ef17e05_3_mwpm_03201609.jpeg","is_content":"1"},{"uniquekey":"d0287d984d0ef4c1938ddb192014a260","title":"石楼：两辆电动车追尾 一人重伤","date":"2021-03-26 14:24:00","category":"头条","author_name":"黄河新闻网吕梁频道","url":"https://mini.eastday.com/mobile/210326142427193862944.html","thumbnail_pic_s":"https://dfzximg02.dftoutiao.com/news/20210326/20210326142427_bfe420db5a131d0a14af6613ad628360_1_mwpm_03201609.jpeg","is_content":"1"},{"uniquekey":"359ae35fce2b97a99821aa009a765a31","title":"约见网友被诈骗 民警追赃终返还","date":"2021-03-26 14:18:00","category":"头条","author_name":"江南时报","url":"https://mini.eastday.com/mobile/210326141819966329651.html","thumbnail_pic_s":"https://dfzximg02.dftoutiao.com/news/20210326/20210326141819_4cdb0ed28fce0bafe5a65e6a89911422_1_mwpm_03201609.png","thumbnail_pic_s02":"https://dfzximg02.dftoutiao.com/news/20210326/20210326141819_4cdb0ed28fce0bafe5a65e6a89911422_2_mwpm_03201609.png","is_content":"1"},{"uniquekey":"94d050b508c42256c6cc38394e0089f5","title":"汇聚金融力量 践行绿色发展\u2014\u2014平安租赁荣获\"陆家嘴金融城绿色金融综合发展平台绿色项目\"认证","date":"2021-03-26 14:17:00","category":"头条","author_name":"消费日报网","url":"https://mini.eastday.com/mobile/210326141728276621281.html","thumbnail_pic_s":"https://dfzximg02.dftoutiao.com/news/20210326/20210326141728_d41d8cd98f00b204e9800998ecf8427e_1_mwpm_03201609.png","is_content":"1"},{"uniquekey":"0249274a04402a9c0787c6438263ea3f","title":"响应中新自贸协定升级，鑫荣懋集团与佳沛公司共庆佳沛新西兰奇异果上市","date":"2021-03-26 14:15:00","category":"头条","author_name":"消费日报网","url":"https://mini.eastday.com/mobile/210326141522769889900.html","thumbnail_pic_s":"https://dfzximg02.dftoutiao.com/news/20210326/20210326141522_d41d8cd98f00b204e9800998ecf8427e_1_mwpm_03201609.png","thumbnail_pic_s02":"https://dfzximg02.dftoutiao.com/news/20210326/20210326141522_d41d8cd98f00b204e9800998ecf8427e_2_mwpm_03201609.png","thumbnail_pic_s03":"https://dfzximg02.dftoutiao.com/news/20210326/20210326141522_d41d8cd98f00b204e9800998ecf8427e_3_mwpm_03201609.png","is_content":"1"},{"uniquekey":"5a6e52bb2acf56ea311bbd9ae176f7b3","title":"\u201c医术精湛高超\u2022创造生命奇迹\u201d\u2014\u2014一面来自患者的特殊锦旗","date":"2021-03-26 14:13:00","category":"头条","author_name":"消费日报网","url":"https://mini.eastday.com/mobile/210326141312479880867.html","thumbnail_pic_s":"https://dfzximg02.dftoutiao.com/news/20210326/20210326141312_d41d8cd98f00b204e9800998ecf8427e_1_mwpm_03201609.png","thumbnail_pic_s02":"https://dfzximg02.dftoutiao.com/news/20210326/20210326141312_d41d8cd98f00b204e9800998ecf8427e_2_mwpm_03201609.png","thumbnail_pic_s03":"https://dfzximg02.dftoutiao.com/news/20210326/20210326141312_d41d8cd98f00b204e9800998ecf8427e_3_mwpm_03201609.png","is_content":"1"},{"uniquekey":"9515c5d879e5fea501595e86dae6b796","title":"不放弃拼到底！今晚，上海男排放手一搏","date":"2021-03-26 14:08:00","category":"头条","author_name":"新民晚报","url":"https://mini.eastday.com/mobile/210326140800856667202.html","thumbnail_pic_s":"https://dfzximg02.dftoutiao.com/news/20210326/20210326140800_7b70a1ce85f6df4904506abb24d4a864_1_mwpm_03201609.jpeg","thumbnail_pic_s02":"https://dfzximg02.dftoutiao.com/news/20210326/20210326140800_7b70a1ce85f6df4904506abb24d4a864_2_mwpm_03201609.jpeg","thumbnail_pic_s03":"https://dfzximg02.dftoutiao.com/news/20210326/20210326140800_7b70a1ce85f6df4904506abb24d4a864_3_mwpm_03201609.jpeg","is_content":"1"},{"uniquekey":"69b4b7c2bba13f698729d15953d62cb1","title":"\u201c十三邀\u201d中青年名家书画交流展在合川美术馆开幕","date":"2021-03-26 14:04:00","category":"头条","author_name":"徐之腾艺术空间","url":"https://mini.eastday.com/mobile/210326140422713749616.html","thumbnail_pic_s":"https://dfzximg02.dftoutiao.com/news/20210326/20210326140422_cb454e4c6e40d9c05c1c0f33238577d3_1_mwpm_03201609.jpeg","thumbnail_pic_s02":"https://dfzximg02.dftoutiao.com/news/20210326/20210326140422_cb454e4c6e40d9c05c1c0f33238577d3_2_mwpm_03201609.jpeg","thumbnail_pic_s03":"https://dfzximg02.dftoutiao.com/news/20210326/20210326140422_cb454e4c6e40d9c05c1c0f33238577d3_3_mwpm_03201609.jpeg","is_content":"1"},{"uniquekey":"a017ea93e3666f745f7b7002d53bee58","title":"女子车祸身亡，留下2590万元天价保单，警方发现事情不简单","date":"2021-03-26 14:03:00","category":"头条","author_name":"新快报","url":"https://mini.eastday.com/mobile/210326140342949732225.html","thumbnail_pic_s":"https://dfzximg02.dftoutiao.com/news/20210326/20210326140342_80aa3bb5dd301480dfb477f76da30c64_1_mwpm_03201609.jpeg","is_content":"1"},{"uniquekey":"76453a71458ab6b89bb418492acb0d3b","title":"多名主播带货遭网友围攻 耐克小姐姐被骂哭","date":"2021-03-26 14:03:00","category":"头条","author_name":"大象新闻","url":"https://mini.eastday.com/mobile/210326140324528810878.html","thumbnail_pic_s":"https://dfzximg02.dftoutiao.com/news/20210326/20210326140324_dee64f99b29ca4cdc98a72a5361d01d3_0_mwpm_03201609.jpeg","thumbnail_pic_s02":"https://dfzximg02.dftoutiao.com/news/20210326/20210326140324_dee64f99b29ca4cdc98a72a5361d01d3_1_mwpm_03201609.jpeg","thumbnail_pic_s03":"https://dfzximg02.dftoutiao.com/news/20210326/20210326140324_dee64f99b29ca4cdc98a72a5361d01d3_2_mwpm_03201609.jpeg","is_content":"1"},{"uniquekey":"59c90f520a13035ec8ff46d393bc16b8","title":"40年喝了过10000斤白酒 男子长出\u201c象鼻子\u201d","date":"2021-03-26 14:02:00","category":"头条","author_name":"辽宁台新闻中心","url":"https://mini.eastday.com/mobile/210326140246161635523.html","thumbnail_pic_s":"https://dfzximg02.dftoutiao.com/news/20210326/20210326140246_9d87536cbff07bd21af109666b947413_1_mwpm_03201609.png","thumbnail_pic_s02":"https://dfzximg02.dftoutiao.com/news/20210326/20210326140246_9d87536cbff07bd21af109666b947413_2_mwpm_03201609.png","is_content":"1"},{"uniquekey":"83566c5419054e13f8deb1874102b67d","title":"南宁哈罗礼德奖学金计划公布 携手剑桥大学\u2014\u2014哈罗优秀校友分享国际化成长之路","date":"2021-03-26 13:57:00","category":"头条","author_name":"中国日报网","url":"https://mini.eastday.com/mobile/210326135701888416387.html","thumbnail_pic_s":"https://dfzximg02.dftoutiao.com/news/20210326/20210326135701_b9343f20d00256e57a339742a7fb898a_0_mwpm_03201609.jpeg","thumbnail_pic_s02":"https://dfzximg02.dftoutiao.com/news/20210326/20210326135701_b9343f20d00256e57a339742a7fb898a_1_mwpm_03201609.jpeg","thumbnail_pic_s03":"https://dfzximg02.dftoutiao.com/news/20210326/20210326135701_b9343f20d00256e57a339742a7fb898a_2_mwpm_03201609.jpeg","is_content":"1"},{"uniquekey":"4e57453a7e0b19606fda3b104b3dbd0b","title":"3月28日卤人甲加盟费限时下调，热门小吃三个月回本","date":"2021-03-26 13:57:00","category":"头条","author_name":"经济网","url":"https://mini.eastday.com/mobile/210326135701117323100.html","thumbnail_pic_s":"https://dfzximg02.dftoutiao.com/news/20210326/20210326135701_cc67e280f3fe771e5caf330cd000cb34_0_mwpm_03201609.jpeg","thumbnail_pic_s02":"https://dfzximg02.dftoutiao.com/news/20210326/20210326135701_cc67e280f3fe771e5caf330cd000cb34_1_mwpm_03201609.png","thumbnail_pic_s03":"https://dfzximg02.dftoutiao.com/news/20210326/20210326135701_cc67e280f3fe771e5caf330cd000cb34_2_mwpm_03201609.png","is_content":"1"},{"uniquekey":"8d24bef810ed97cc3410adc68633d6cf","title":"出口易完成亚马逊MFN轨迹对接，助力卖家轻松发运！","date":"2021-03-26 13:57:00","category":"头条","author_name":"经济网","url":"https://mini.eastday.com/mobile/210326135700134473326.html","thumbnail_pic_s":"https://dfzximg02.dftoutiao.com/news/20210326/20210326135700_6e3ac02a041b26da68c1277513edc0d2_1_mwpm_03201609.png","thumbnail_pic_s02":"https://dfzximg02.dftoutiao.com/news/20210326/20210326135700_6e3ac02a041b26da68c1277513edc0d2_2_mwpm_03201609.jpeg","thumbnail_pic_s03":"https://dfzximg02.dftoutiao.com/news/20210326/20210326135700_6e3ac02a041b26da68c1277513edc0d2_3_mwpm_03201609.jpeg","is_content":"1"},{"uniquekey":"dbce57253494673b229155a94c58a3e5","title":"疫情逆势增长，土豆教育重磅发布全能课程体系3.0","date":"2021-03-26 13:56:00","category":"头条","author_name":"国际在线","url":"https://mini.eastday.com/mobile/210326135659569749375.html","thumbnail_pic_s":"https://dfzximg02.dftoutiao.com/news/20210326/20210326135659_264e08d1c86ea5f239636353af860b94_0_mwpm_03201609.png","thumbnail_pic_s02":"https://dfzximg02.dftoutiao.com/news/20210326/20210326135659_264e08d1c86ea5f239636353af860b94_1_mwpm_03201609.jpeg","thumbnail_pic_s03":"https://dfzximg02.dftoutiao.com/news/20210326/20210326135659_264e08d1c86ea5f239636353af860b94_2_mwpm_03201609.png","is_content":"1"},{"uniquekey":"6f2908ce92d06292144c90a2b528da13","title":"美图2020年财报：营收大增22.1%，收入结构更加健康","date":"2021-03-26 13:56:00","category":"头条","author_name":"凤凰网","url":"https://mini.eastday.com/mobile/210326135658883182782.html","thumbnail_pic_s":"https://dfzximg02.dftoutiao.com/news/20210326/20210326135658_6cb4692239b37f119d03a3c3abef5b5e_0_mwpm_03201609.jpg","is_content":"1"},{"uniquekey":"2565f127eeb40c6a55ee3ad0237499a2","title":"人瑞人才（6919.HK）荣获金帜奖\u201c2021最佳人力资源综合服务供应商\u201d","date":"2021-03-26 13:56:00","category":"头条","author_name":"中国金融网","url":"https://mini.eastday.com/mobile/210326135657822598871.html","thumbnail_pic_s":"https://dfzximg02.dftoutiao.com/news/20210326/20210326135657_82dc4f0a4702ac5b366b1027cbb329b8_0_mwpm_03201609.jpeg","is_content":"1"},{"uniquekey":"62db77d8ec0b3f83c7a4fa6cb991d17c","title":"打破常规才能创新突破，润初妍益生菌水乳即将上市！","date":"2021-03-26 13:56:00","category":"头条","author_name":"中国资讯网","url":"https://mini.eastday.com/mobile/210326135657370709287.html","thumbnail_pic_s":"https://dfzximg02.dftoutiao.com/news/20210326/20210326135657_91bc6f24111249551b195f233949543e_0_mwpm_03201609.png","thumbnail_pic_s02":"https://dfzximg02.dftoutiao.com/news/20210326/20210326135657_91bc6f24111249551b195f233949543e_1_mwpm_03201609.png","thumbnail_pic_s03":"https://dfzximg02.dftoutiao.com/news/20210326/20210326135657_91bc6f24111249551b195f233949543e_2_mwpm_03201609.png","is_content":"1"},{"uniquekey":"d49d4a807733605f433ae248ef310730","title":"健康证下来了，我和股市都是健康的","date":"2021-03-26 13:53:00","category":"头条","author_name":"年糕猫","url":"https://mini.eastday.com/mobile/210326135344666237388.html","thumbnail_pic_s":"https://dfzximg02.dftoutiao.com/news/20210326/20210326135344_96db3334b24411f9837c127471acdcd9_1_mwpm_03201609.jpeg","is_content":"1"},{"uniquekey":"4fb08033935000d9a57bab1a22311c0f","title":"\u201c无\u201d中 生\u201c有\u201d\u2014\u2014马路炁象作品展将于西安崔振宽美术馆开展","date":"2021-03-26 13:48:00","category":"头条","author_name":"国画家","url":"https://mini.eastday.com/mobile/210326134851551994895.html","thumbnail_pic_s":"https://dfzximg02.dftoutiao.com/news/20210326/20210326134851_219b2414e87176d44e9cef071d6e2095_1_mwpm_03201609.jpeg","thumbnail_pic_s02":"https://dfzximg02.dftoutiao.com/news/20210326/20210326134851_219b2414e87176d44e9cef071d6e2095_2_mwpm_03201609.jpeg","thumbnail_pic_s03":"https://dfzximg02.dftoutiao.com/news/20210326/20210326134851_219b2414e87176d44e9cef071d6e2095_3_mwpm_03201609.jpeg","is_content":"1"},{"uniquekey":"573145e5f6de7aecdb761b66253b7efa","title":"\u201c头条寻人\u201d助走失30年女子回家，并捐助万元济困","date":"2021-03-26 13:48:00","category":"头条","author_name":"消费日报网","url":"https://mini.eastday.com/mobile/210326134810605851754.html","thumbnail_pic_s":"https://dfzximg02.dftoutiao.com/news/20210326/20210326134810_d41d8cd98f00b204e9800998ecf8427e_1_mwpm_03201609.png","is_content":"1"},{"uniquekey":"a1fc48c2a2822b486e540d6ddfb1c013","title":"「枫警故事」老人报案丢失财物 民警相助\u201c失\u201d而复得","date":"2021-03-26 13:47:00","category":"头条","author_name":"消费日报网","url":"https://mini.eastday.com/mobile/210326134734976562479.html","thumbnail_pic_s":"https://dfzximg02.dftoutiao.com/news/20210326/20210326134734_d41d8cd98f00b204e9800998ecf8427e_1_mwpm_03201609.png","thumbnail_pic_s02":"https://dfzximg02.dftoutiao.com/news/20210326/20210326134734_d41d8cd98f00b204e9800998ecf8427e_2_mwpm_03201609.png","thumbnail_pic_s03":"https://dfzximg02.dftoutiao.com/news/20210326/20210326134734_d41d8cd98f00b204e9800998ecf8427e_3_mwpm_03201609.png","is_content":"1"},{"uniquekey":"94221b2f65bc49cd005a3936d99411d6","title":"基站成\u201c邻居\u201d太吵了 铁塔公司:物业收高额场租","date":"2021-03-26 13:47:00","category":"头条","author_name":"看看新闻Knews","url":"https://mini.eastday.com/mobile/210326134708087424846.html","thumbnail_pic_s":"https://dfzximg02.dftoutiao.com/news/20210326/20210326134708_54af9f94fc7c79c25a7b59ea2a64a46c_1_mwpm_03201609.png","thumbnail_pic_s02":"https://dfzximg02.dftoutiao.com/news/20210326/20210326134708_54af9f94fc7c79c25a7b59ea2a64a46c_2_mwpm_03201609.png","thumbnail_pic_s03":"https://dfzximg02.dftoutiao.com/news/20210326/20210326134708_54af9f94fc7c79c25a7b59ea2a64a46c_3_mwpm_03201609.png","is_content":"1"},{"uniquekey":"597ad4b782b6c965a0165d2903b66718","title":"\u201c梅姨案\u201d受害者申军良索赔480万，儿子仍在适应新生活","date":"2021-03-26 13:43:00","category":"头条","author_name":"中国新闻周刊","url":"https://mini.eastday.com/mobile/210326134308036311736.html","thumbnail_pic_s":"https://dfzximg02.dftoutiao.com/news/20210326/20210326134308_46dadcbf0940b5f65b4422abc430451f_1_mwpm_03201609.jpeg","thumbnail_pic_s02":"https://dfzximg02.dftoutiao.com/news/20210326/20210326134308_46dadcbf0940b5f65b4422abc430451f_2_mwpm_03201609.jpeg","thumbnail_pic_s03":"https://dfzximg02.dftoutiao.com/news/20210326/20210326134308_46dadcbf0940b5f65b4422abc430451f_3_mwpm_03201609.jpeg","is_content":"1"},{"uniquekey":"2da6d4b1b906e3fd7e79be3b91614627","title":"跨界营销，COLMO洗碗机净洗之夜如何借\u201c脱口秀+红酒品鉴\u201d实现突围？","date":"2021-03-26 13:41:00","category":"头条","author_name":"中国网","url":"https://mini.eastday.com/mobile/210326134153209213476.html","thumbnail_pic_s":"https://dfzximg02.dftoutiao.com/news/20210326/20210326134153_6a34f7753263fdfc0480612aa3d69e6a_0_mwpm_03201609.jpeg","thumbnail_pic_s02":"https://dfzximg02.dftoutiao.com/news/20210326/20210326134153_6a34f7753263fdfc0480612aa3d69e6a_1_mwpm_03201609.jpeg","thumbnail_pic_s03":"https://dfzximg02.dftoutiao.com/news/20210326/20210326134153_6a34f7753263fdfc0480612aa3d69e6a_2_mwpm_03201609.jpeg","is_content":"1"},{"uniquekey":"d65707fd8735ff6b6670b84ab51ae287","title":"通过性传播的4种性病，第一种最可怕！思想开放的人要小心了","date":"2021-03-26 13:39:00","category":"头条","author_name":"家庭医生名医在线","url":"https://mini.eastday.com/mobile/210326133930767621320.html","thumbnail_pic_s":"https://dfzximg02.dftoutiao.com/news/20210326/20210326133930_7ba7a4c122fb41c1d6885e96464f7dfe_1_mwpm_03201609.jpeg","thumbnail_pic_s02":"https://dfzximg02.dftoutiao.com/news/20210326/20210326133930_7ba7a4c122fb41c1d6885e96464f7dfe_2_mwpm_03201609.jpeg","thumbnail_pic_s03":"https://dfzximg02.dftoutiao.com/news/20210326/20210326133930_7ba7a4c122fb41c1d6885e96464f7dfe_3_mwpm_03201609.jpeg","is_content":"1"},{"uniquekey":"12eda6e206a992b3af9065d9ba54633e","title":"张维平拐卖儿童案二审开庭，受害家属：不希望人贩子被立即执行死刑","date":"2021-03-26 13:37:00","category":"头条","author_name":"新快报","url":"https://mini.eastday.com/mobile/210326133726833226433.html","thumbnail_pic_s":"https://dfzximg02.dftoutiao.com/news/20210326/20210326133726_c1e58ade89a0384c00184de2cfc04d2b_1_mwpm_03201609.jpeg","thumbnail_pic_s02":"https://dfzximg02.dftoutiao.com/news/20210326/20210326133726_c1e58ade89a0384c00184de2cfc04d2b_2_mwpm_03201609.jpeg","thumbnail_pic_s03":"https://dfzximg02.dftoutiao.com/news/20210326/20210326133726_c1e58ade89a0384c00184de2cfc04d2b_3_mwpm_03201609.jpeg","is_content":"1"},{"uniquekey":"f4670394cdc24a291ab2caa936168d66","title":"第六届浙江大学创业大赛北部赛区决赛暨颁奖典礼在首科大厦成功举办","date":"2021-03-26 13:28:00","category":"头条","author_name":"丰台组工","url":"https://mini.eastday.com/mobile/210326132847650623979.html","thumbnail_pic_s":"https://dfzximg02.dftoutiao.com/news/20210326/20210326132847_23088687eae25f8899f8914a834972d1_0_mwpm_03201609.jpeg","thumbnail_pic_s02":"https://dfzximg02.dftoutiao.com/news/20210326/20210326132847_23088687eae25f8899f8914a834972d1_1_mwpm_03201609.jpeg","thumbnail_pic_s03":"https://dfzximg02.dftoutiao.com/news/20210326/20210326132847_23088687eae25f8899f8914a834972d1_2_mwpm_03201609.jpeg","is_content":"1"}]
         * page : 1
         * pageSize : 30
         */

        @SerializedName("stat")
        private String stat;
        @SerializedName("data")
        private List<DataDTO> data;
        @SerializedName("page")
        private String page;
        @SerializedName("pageSize")
        private String pageSize;

        public String getStat() {
            return stat;
        }

        public void setStat(String stat) {
            this.stat = stat;
        }

        public List<DataDTO> getData() {
            return data;
        }

        public void setData(List<DataDTO> data) {
            this.data = data;
        }

        public String getPage() {
            return page;
        }

        public void setPage(String page) {
            this.page = page;
        }

        public String getPageSize() {
            return pageSize;
        }

        public void setPageSize(String pageSize) {
            this.pageSize = pageSize;
        }

        public static class DataDTO {
            /**
             * uniquekey : b786faff07c51ecd019d83149a2277e6
             * title : 喜讯！正荣地产荣获“绿色金融发行后认证证书”
             * date : 2021-03-26 14:29:00
             * category : 头条
             * author_name : “娱”我同行
             * url : https://mini.eastday.com/mobile/210326142911140295956.html
             * thumbnail_pic_s : https://dfzximg02.dftoutiao.com/news/20210326/20210326142911_bb323782361f4205144e7b6d7090c50f_1_mwpm_03201609.png
             * thumbnail_pic_s02 : https://dfzximg02.dftoutiao.com/news/20210326/20210326142911_bb323782361f4205144e7b6d7090c50f_2_mwpm_03201609.png
             * thumbnail_pic_s03 : https://dfzximg02.dftoutiao.com/news/20210326/20210326142911_bb323782361f4205144e7b6d7090c50f_3_mwpm_03201609.png
             * is_content : 1
             */

            @SerializedName("uniquekey")
            private String uniquekey;
            @SerializedName("title")
            private String title;
            @SerializedName("date")
            private String date;
            @SerializedName("category")
            private String category;
            @SerializedName("author_name")
            private String authorName;
            @SerializedName("url")
            private String url;
            @SerializedName("thumbnail_pic_s")
            private String thumbnailPicS;
            @SerializedName("thumbnail_pic_s02")
            private String thumbnailPicS02;
            @SerializedName("thumbnail_pic_s03")
            private String thumbnailPicS03;
            @SerializedName("is_content")
            private String isContent;

            public String getUniquekey() {
                return uniquekey;
            }

            public void setUniquekey(String uniquekey) {
                this.uniquekey = uniquekey;
            }

            public String getTitle() {
                return title;
            }

            public void setTitle(String title) {
                this.title = title;
            }

            public String getDate() {
                return date;
            }

            public void setDate(String date) {
                this.date = date;
            }

            public String getCategory() {
                return category;
            }

            public void setCategory(String category) {
                this.category = category;
            }

            public String getAuthorName() {
                return authorName;
            }

            public void setAuthorName(String authorName) {
                this.authorName = authorName;
            }

            public String getUrl() {
                return url;
            }

            public void setUrl(String url) {
                this.url = url;
            }

            public String getThumbnailPicS() {
                return thumbnailPicS;
            }

            public void setThumbnailPicS(String thumbnailPicS) {
                this.thumbnailPicS = thumbnailPicS;
            }

            public String getThumbnailPicS02() {
                return thumbnailPicS02;
            }

            public void setThumbnailPicS02(String thumbnailPicS02) {
                this.thumbnailPicS02 = thumbnailPicS02;
            }

            public String getThumbnailPicS03() {
                return thumbnailPicS03;
            }

            public void setThumbnailPicS03(String thumbnailPicS03) {
                this.thumbnailPicS03 = thumbnailPicS03;
            }

            public String getIsContent() {
                return isContent;
            }

            public void setIsContent(String isContent) {
                this.isContent = isContent;
            }
        }
    }
}
