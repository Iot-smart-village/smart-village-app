package com.example.smartvillage.cunwei;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.example.smartvillage.R;
import com.example.smartvillage.model.Notice;

import cn.bmob.v3.Bmob;
import cn.bmob.v3.exception.BmobException;
import cn.bmob.v3.listener.SaveListener;

public class NoticeActivity extends AppCompatActivity {
EditText editText;
Button button;
public static String APPID = "6fe8eb567bb46bfa61a97578a8e41206";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_notice);
        Bmob.initialize(this,APPID);
//        BmobPush.startWork(this, APPID);
        editText = findViewById(R.id.notice_edit);
        button = findViewById(R.id.notice_post);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Notice leader = new Notice();
                leader.setNotice(editText.getText().toString());
                leader.save(new SaveListener<String>() {
                    @Override
                    public void done(String s, BmobException e) {
                        if (e == null) {
                            Toast.makeText(NoticeActivity.this,"发布成功！",Toast.LENGTH_SHORT).show();
                        } else {
                            Toast.makeText(NoticeActivity.this,"发布失败！",Toast.LENGTH_SHORT).show();
                        }
                    }
                });

            }
        });
    }
}