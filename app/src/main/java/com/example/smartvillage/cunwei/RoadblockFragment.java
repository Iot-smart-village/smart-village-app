package com.example.smartvillage.cunwei;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;

import com.example.smartvillage.MainViewModel;
import com.example.smartvillage.R;
import com.example.smartvillage.onenet.MyOneNetApi;
import com.google.android.material.bottomsheet.BottomSheetDialogFragment;
import com.google.android.material.button.MaterialButton;
import com.google.android.material.textview.MaterialTextView;
import com.google.gson.JsonArray;

import java.util.Objects;

public class RoadblockFragment extends BottomSheetDialogFragment {

    private static final String TAG = "路障系统";

    MainViewModel model;
    MyOneNetApi myOneNetApi;

    MaterialButton btn_open, btn_close;
    MaterialTextView tv_door,tv_visitor;

    public RoadblockFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        model = new ViewModelProvider(requireActivity()).get(MainViewModel.class);
        myOneNetApi = Objects.requireNonNull(model.getMyOneNetApi().getValue());

        //每次进入更新数据
        boolean[] newRoadBlockStates = new boolean[1];
        String dataStreamIds = "路障";
        myOneNetApi.getDataStreams(dataStreamIds, response -> {
            if (response.body() != null && response.body().get("errno").getAsInt() == 0) {
                JsonArray obj = response.body().get("data").getAsJsonArray();
                newRoadBlockStates[0] = (obj.get(0).getAsJsonObject().get("current_value").getAsInt() == 1);
                Log.d(TAG, "获取的路障系统状态：" + newRoadBlockStates[0]);
                model.setRoadblockState(newRoadBlockStates[0]);
            }
        });
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view =  inflater.inflate(R.layout.fragment_roadblock, container, false);

        btn_open = view.findViewById(R.id.door_security_open);
        btn_close = view.findViewById(R.id.door_security_close);
        tv_door = view.findViewById(R.id.door_security_door);
        tv_visitor = view.findViewById(R.id.door_security_visitor);

        btn_open.setOnClickListener(btnOpenClickListener);
        btn_close.setOnClickListener(btnCloseClickListener);

        model.getRoadblockState().observe(this, roadblockStatesObserver);

        return view;
    }

    //上升按钮监听
    View.OnClickListener btnOpenClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            //在线命令
            myOneNetApi.sendCommand("roadblock:1", response -> {
                if (response.body() != null && response.body().get("errno").getAsInt() == 0) {
                    Toast.makeText(getContext(), "成功创建命令", Toast.LENGTH_SHORT).show();

                    //更新ViewModel中的数据
                    model.setRoadblockState(true);
                }
                else if (response.body().get("errno").getAsInt() == 10) {
                    Toast.makeText(getContext(), "设备不在线", Toast.LENGTH_SHORT).show();
                }
            });
        }
    };
    //下降按钮监听
    View.OnClickListener btnCloseClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            //在线命令
            myOneNetApi.sendCommand("roadblock:0", response -> {
                if (response.body() != null && response.body().get("errno").getAsInt() == 0) {
                    Toast.makeText(getContext(), "成功创建命令", Toast.LENGTH_SHORT).show();

                    //更新ViewModel中的数据
                    model.setRoadblockState(false);
                }
                else if (response.body().get("errno").getAsInt() == 10) {
                    Toast.makeText(getContext(), "设备不在线", Toast.LENGTH_SHORT).show();
                }
            });
        }
    };

    Observer<Boolean> roadblockStatesObserver = new Observer<Boolean>() {
        @Override
        public void onChanged(Boolean aBoolean) {
            Log.d(TAG, "ViewModel中路障状态改变：" + aBoolean);
            tv_door.setText(aBoolean?"已升起":"已降下");
            tv_door.setTextColor(getResources().getColor(aBoolean?R.color.colorPrimary:R.color.darker_gray));
        }
    };
}