package com.example.smartvillage.cunwei;

import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ExpandableListView;
import android.widget.Toast;

import androidx.appcompat.widget.AppCompatToggleButton;
import androidx.core.content.ContextCompat;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;

import com.example.smartvillage.MainViewModel;
import com.example.smartvillage.R;
import com.example.smartvillage.adapters.ExpandableListAdapter;
import com.example.smartvillage.onenet.MyOneNetApi;
import com.google.android.material.bottomsheet.BottomSheetDialogFragment;
import com.google.android.material.switchmaterial.SwitchMaterial;
import com.google.gson.JsonArray;

import java.util.Objects;

public class StreetLampFragment extends BottomSheetDialogFragment {

    private static final String TAG = "灯光控制";

    MainViewModel model;
    MyOneNetApi myOneNetApi;

    String[] locations = {"爱国路","敬业路","诚信路","友善路"};
    String[][] bulbs = {{"路灯1","路灯2","路灯3","路灯4","路灯5"},{"路灯6","路灯7","路灯8"},{"路灯9","路灯10","路灯11"},{"路灯11","路灯12","路灯13"}};
    boolean[][] allBulbStates = {{false,false,false,false,false},{false,false,false},{false,false,false},{false,false,false}};
    ExpandableListAdapter lightListAdapter;

    ExpandableListView expandableListView;

    public StreetLampFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        model = new ViewModelProvider(requireActivity()).get(MainViewModel.class);
        myOneNetApi = Objects.requireNonNull(model.getMyOneNetApi().getValue());

        //每次进入更新数据
        boolean[] newBulbStates = new boolean[3];
        String ledId = "灯光1控制,灯光2控制,灯光3控制";
        myOneNetApi.getDataStreams(ledId, response -> {
            if (response.body() != null && response.body().get("errno").getAsInt() == 0) {
                JsonArray obj = response.body().get("data").getAsJsonArray();
                newBulbStates[0] = (obj.get(0).getAsJsonObject().get("current_value").getAsInt() == 1);
                newBulbStates[1] = (obj.get(1).getAsJsonObject().get("current_value").getAsInt() == 1);
                newBulbStates[2] = (obj.get(2).getAsJsonObject().get("current_value").getAsInt() == 1);
                Log.d(TAG, "获取的灯光状态" + newBulbStates[0] + newBulbStates[1] + newBulbStates[2]);
                model.setStreetLampStates(newBulbStates);
            }
        });

        lightListAdapter = new ExpandableListAdapter(locations, bulbs, allBulbStates, getContext());
        lightListAdapter.setChildClickListener(childClickListener);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_street_lamp, container, false);

        expandableListView = view.findViewById(R.id.light_list);

        expandableListView.setAdapter(lightListAdapter);

        model.getStreetLampStates().observe(this, bulbStatesObserver);

        return view;
    }

    Observer<boolean[]> bulbStatesObserver = new Observer<boolean[]>() {
        @Override
        public void onChanged(boolean[] bulbStates) {
            Log.d(TAG, "ModelView的路灯状态"+bulbStates[0]+bulbStates[1]+bulbStates[2]);
            boolean[][] newBulbStates = {{bulbStates[0],bulbStates[1],bulbStates[2],false,false},{false,false,false},{false,false,false},{false,false,false}};
            lightListAdapter.setChildValues(newBulbStates);
            lightListAdapter.notifyDataSetChanged();
        }
    };

    ExpandableListAdapter.ChildClickListener childClickListener = new ExpandableListAdapter.ChildClickListener() {
        @Override
        public void OnChildClick(View view, int groupPosition, int childPosition) {
            Log.d(TAG, "你切换了"+locations[groupPosition]+"中的"+bulbs[groupPosition][childPosition]);

            AppCompatToggleButton clickedButton = (AppCompatToggleButton)view;
            boolean isChecked = clickedButton.isChecked();

            String command;
            Drawable drawable = ContextCompat.getDrawable(requireContext(), R.drawable.ic_round_lightbulb_24);

            if (groupPosition == 0) {
                String lampNo=null;
                switch (childPosition) {
                    case 0:
                        lampNo = "1";
                        break;
                    case 1:
                        lampNo = "2";
                        break;
                    case 2:
                        lampNo = "3";
                        break;
                    default:
                        lampNo = "0";
                }
                if(lampNo.equals("1")) {
                    command = "abc" + ":" + (isChecked ? "1" : "0");
                    Log.d(TAG, "发送的命令："+command);
                    boolean[] states = model.getStreetLampStates().getValue();

                    myOneNetApi.sendCommand(0, 0, command, response -> {

                        if (response.body() != null && response.body().get("errno").getAsInt() == 0) {
                            Toast.makeText(getContext(), "成功创建命令", Toast.LENGTH_SHORT).show();

                            //更新ViewModel中的数据
                            states[childPosition] = isChecked;
                            model.setStreetLampStates(states);
                        } else if (response.body().get("errno").getAsInt() == 10) {
                            ((AppCompatToggleButton) view).setChecked(!isChecked);
                            Toast.makeText(getContext(), "设备不在线", Toast.LENGTH_SHORT).show();
                        }
                        //可以控制的灯更新视图
                        if (clickedButton.isChecked()) {
                            drawable.setTint(getResources().getColor(R.color.white));
                            clickedButton.setTextColor(getResources().getColor(R.color.white));
                        }
                        else {
                            drawable.setTint(getResources().getColor(R.color.darker_gray));
                            clickedButton.setTextColor(getResources().getColor(R.color.darker_gray));
                        }
                        clickedButton.setCompoundDrawablesRelativeWithIntrinsicBounds(drawable,null,null,null);
                    });
                }
                else if (lampNo.equals("2") || lampNo.equals("3")) {
                    command = "LED" + lampNo + "1:" + (isChecked ? "1" : "0");
                    Log.d(TAG, "发送的命令："+command);
                    boolean[] states = model.getStreetLampStates().getValue();

                    myOneNetApi.sendCommand(0, 0, command, response -> {

                        if (response.body() != null && response.body().get("errno").getAsInt() == 0) {
                            Toast.makeText(getContext(), "成功创建命令", Toast.LENGTH_SHORT).show();

                            //更新ViewModel中的数据
                            states[childPosition] = isChecked;
                            model.setStreetLampStates(states);
                        } else if (response.body().get("errno").getAsInt() == 10) {
                            ((AppCompatToggleButton) view).setChecked(!isChecked);
                            Toast.makeText(getContext(), "设备不在线", Toast.LENGTH_SHORT).show();
                        }
                        //可以控制的灯更新视图
                        if (clickedButton.isChecked()) {
                            drawable.setTint(getResources().getColor(R.color.white));
                            clickedButton.setTextColor(getResources().getColor(R.color.white));
                        }
                        else {
                            drawable.setTint(getResources().getColor(R.color.darker_gray));
                            clickedButton.setTextColor(getResources().getColor(R.color.darker_gray));
                        }
                        clickedButton.setCompoundDrawablesRelativeWithIntrinsicBounds(drawable,null,null,null);
                    });
                }
            }
            //所有灯更新视图
            if (clickedButton.isChecked()) {
                drawable.setTint(getResources().getColor(R.color.white));
                clickedButton.setTextColor(getResources().getColor(R.color.white));
            }
            else {
                drawable.setTint(getResources().getColor(R.color.darker_gray));
                clickedButton.setTextColor(getResources().getColor(R.color.darker_gray));
            }
            clickedButton.setCompoundDrawablesRelativeWithIntrinsicBounds(drawable,null,null,null);
        }
    };
}