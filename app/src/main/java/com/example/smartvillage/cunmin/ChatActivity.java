package com.example.smartvillage.cunmin;

import android.annotation.TargetApi;
import android.content.Context;
import android.hardware.Camera;
import android.os.Build;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.smartvillage.R;
import com.example.smartvillage.TestData;
import com.example.smartvillage.adapters.ChatAdapter;
import com.example.smartvillage.model.ChatModel;
import com.example.smartvillage.model.ItemModel;

import java.util.ArrayList;

//import android.support.*;



public class ChatActivity extends AppCompatActivity {

    private RecyclerView recyclerView;
    private ChatAdapter adapter;
    private EditText et;
    private TextView tvSend,tvAdd;
    private String content;

    private Camera camera;
    private boolean ispreview = false;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chat);
        recyclerView = findViewById(R.id.rv);
        et = (EditText) findViewById(R.id.et);
        tvSend = (TextView) findViewById(R.id.tvSend);
        tvAdd = (TextView) findViewById(R.id.add);
        //picture = (SurfaceView) findViewById(R.id.picture);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));
        recyclerView.setAdapter(adapter = new ChatAdapter());
        adapter.replaceAll(TestData.getTestAdData());
     //   getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
        //        WindowManager.LayoutParams.FLAG_FULLSCREEN);
        //写在onCreat方法里面
 //       final SurfaceView surface = findViewById(R.id.surfaceview);//获取surfaceview
 //       final SurfaceHolder surfaceHolder = surface.getHolder();//获取suafaceholder
//        surfaceHolder.setType(SurfaceHolder.SURFACE_TYPE_PUSH_BUFFERS);

        initData();
    }

    private void initData() {
        et.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                content = s.toString().trim();
            }
        });

        tvSend.setOnClickListener(new View.OnClickListener() {
            @TargetApi(Build.VERSION_CODES.M)
            @Override
            public void onClick(View v) {
                ArrayList<ItemModel> data = new ArrayList<>();
                ChatModel model = new ChatModel();
                model.setIcon("http://img.my.csdn.net/uploads/201508/05/1438760758_6667.jpg");
                model.setContent(content);
                data.add(new ItemModel(ItemModel.CHAT_B, model));
                adapter.addAll(data);
                et.setText("");
                hideKeyBorad(et);
            }
        });

//        tvAdd.setOnClickListener(new View.OnClickListener() {
//            @TargetApi(Build.VERSION_CODES.M)
//            @Override
//            public void onClick(View v) {
////                ArrayList<ItemModel> data = new ArrayList<>();
////                ChatModel model = new ChatModel();
////                model.setIcon("http://img.my.csdn.net/uploads/201508/05/1438760758_6667.jpg");
////                model.setContent(content);
////                data.add(new ItemModel(ItemModel.CHAT_B, model));
////                adapter.addAll(data);
////                et.setText("");
////                hideKeyBorad(et);
//                //创建File对象，用于存储拍照后的图片
//                //getExternalCacheDir()方法将图片存放在应用关联缓存目录下,也就是/sdcard/Android/data/应用的包名/cache中
//                if (!ispreview) {//如果没打开
//                    camera = Camera.open();//打开摄像头
//                    ispreview = true;
//                }
//                try {
//                    camera.setPreviewDisplay(surfaceHolder);//设置预览
//                    Camera.Parameters parameters = camera.getParameters();//获取摄像头参数
//
//                    parameters.setPictureFormat(PixelFormat.JPEG);//设置图片为jpg
//                    parameters.set("jpeg-quality", 80);//设置图片质量
//
//                    camera.setParameters(parameters);//重新设置摄像头参数
//                    camera.startPreview();//开始预览
//                    camera.setDisplayOrientation(90);//不加的话，预览的图像就是横的
//                    camera.autoFocus(null);//自动对焦
//
//                } catch (IOException e) {
//                    e.printStackTrace();
//                }
//            }
//        });
//    }
//
//    final Camera.PictureCallback jpeg = new Camera.PictureCallback() {
//        @Override
//        public void onPictureTaken(byte[] data, Camera camera) {
//            Bitmap bitmap = BitmapFactory.decodeByteArray(data, 0, data.length);
//            //根据拍照得到的数据集创建位图
//            camera.stopPreview();
//            ispreview = false;
//            File appDir = new File(Environment.getExternalStorageDirectory(), "/DCIM/Camera/");
//            if (!appDir.exists()) {//如果目录不存在
//                appDir.mkdir();//创建目录
//            }
//            String filename = System.currentTimeMillis() + ".jpg";
//            File file = new File(appDir, filename);
//
//            try {
//                FileOutputStream outputStream = new FileOutputStream(file);
//                //创建文件输出流
//                bitmap.compress(Bitmap.CompressFormat.JPEG, 100, outputStream);
//                //将图片压缩成JPEG格式输出到输出流
//                outputStream.flush();//将缓冲区的数据都输入到输出流
//                outputStream.close();//关闭输出流
//            } catch (FileNotFoundException e) {
//                e.printStackTrace();
//            } catch (IOException e) {
//                e.printStackTrace();
//            }
//
//            //将图片插入到系统图库
//            try {
//                MediaStore.Images.Media.insertImage(ChatActivity.this.getContentResolver(), file.getAbsolutePath(), filename, null);
//            } catch (FileNotFoundException e) {
//                e.printStackTrace();
//            }
//
//            //通知图库更新
//            ChatActivity.this.sendBroadcast(new Intent(Intent.
//                    ACTION_MEDIA_SCANNER_SCAN_FILE, Uri.parse("file://"+"")));
//            Toast.makeText(ChatActivity.this,"照片已存"+file,Toast.LENGTH_LONG).show();
//            resetCamera();//图片保存后，判断，是否需要重新打开预览，重新创建一个方法，第8步
//        }
//    };
//
//    private void resetCamera() {
//        if (!ispreview) {
//            camera.startPreview();
//            ispreview=true;
//        }
//    }
//
//    protected  void onPause() {
//        super.onPause();
//        if (camera != null) {
//            camera.stopPreview();
//            camera.release();
//        }
    }
        private void hideKeyBorad (View v) {
            InputMethodManager imm = (InputMethodManager) v.getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
            if (imm.isActive()) {
                imm.hideSoftInputFromWindow(v.getApplicationWindowToken(), 0);
            }
        }
}