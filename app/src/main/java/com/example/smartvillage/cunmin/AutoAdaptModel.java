package com.example.smartvillage.cunmin;

public class AutoAdaptModel {

    private boolean[] states;
    private int[] values;

    public AutoAdaptModel(boolean[] states, int[] values) {
        this.states = states;
        this.values = values;
    }

    public boolean[] getStates() {
        return states;
    }

    public void setState(boolean state, int pos) {
        this.states[pos] = state;
    }

    public int[] getValues() {
        return values;
    }

    public void setValue(int value, int pos) {
        this.values[pos] = value;
    }

}
