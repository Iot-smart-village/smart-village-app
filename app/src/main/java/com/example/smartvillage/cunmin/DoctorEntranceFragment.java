package com.example.smartvillage.cunmin;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;

import com.example.smartvillage.R;
import com.google.android.material.bottomsheet.BottomSheetDialogFragment;

public class DoctorEntranceFragment extends BottomSheetDialogFragment {
    Button button1,button2;
    public DoctorEntranceFragment(){}
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_doctor_entrance, container, false);
        button1 = view.findViewById(R.id.video_doctor);
        button2 = view.findViewById(R.id.appointment);
        return view;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        button1.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                Intent intent1 = new Intent(requireActivity(), ChatActivity.class);
                DoctorEntranceFragment.super.getContext().startActivity(intent1);
            }
        });

        button2.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                Intent intent2 = new Intent(requireActivity(), ScheduleActivity.class);
                DoctorEntranceFragment.super.getContext().startActivity(intent2);
            }
        });
    }

}