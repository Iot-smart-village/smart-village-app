package com.example.smartvillage.cunmin;

import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ExpandableListView;
import android.widget.Toast;

import androidx.appcompat.widget.AppCompatToggleButton;
import androidx.core.content.ContextCompat;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;

import com.example.smartvillage.MainViewModel;
import com.example.smartvillage.R;
import com.example.smartvillage.adapters.ExpandableListAdapter;
import com.example.smartvillage.onenet.MyOneNetApi;
import com.google.android.material.bottomsheet.BottomSheetDialogFragment;
import com.google.android.material.switchmaterial.SwitchMaterial;

import java.util.Objects;

public class WindowControlFragment extends BottomSheetDialogFragment {

    private static final String TAG = "窗户控制";

    MainViewModel model;
    MyOneNetApi myOneNetApi;

    String[] locations = {"客厅","卧室","厨房"};
    String[][] windows = {{"窗户1","窗户2","窗户3"},{"窗户1","窗户2"},{"窗户1","窗户2"}};
    boolean[][] allWindowsStates = {{false,false,false},{false,false,false},{false,false}};
    ExpandableListAdapter windowListAdapter;

    ExpandableListView expandableListView;

    public WindowControlFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        model = new ViewModelProvider(requireActivity()).get(MainViewModel.class);
        myOneNetApi = Objects.requireNonNull(model.getMyOneNetApi().getValue());

        //每次进入更新数据
        String windowId = "窗户1";
        myOneNetApi.getDataStreams(windowId, response -> {
            boolean[] newWindowsStates = new boolean[1];
            newWindowsStates[0] = (response.body().get("data").getAsJsonArray().get(0).getAsJsonObject().get("current_value").getAsInt() == 1);
            Log.d(TAG, "获取的阳台状态："+newWindowsStates[0]);
            model.setWindowStates(newWindowsStates);
        });

        windowListAdapter = new ExpandableListAdapter(locations, windows, allWindowsStates, getContext());
        windowListAdapter.setChildClickListener(childClickListener);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_window_control, container, false);

        expandableListView = view.findViewById(R.id.window_list);

        expandableListView.setAdapter(windowListAdapter);

        model.getWindowStates().observe(requireActivity(), windowStatesObserver);

        return view;
    }

    Observer<boolean[]> windowStatesObserver = new Observer<boolean[]>() {
        @Override
        public void onChanged(boolean[] windowStates) {
            Log.d(TAG, "onChanged: "+windowStates[0]);
            allWindowsStates = new boolean[][]{{windowStates[0], false, false}, {false, false}, {false, false}};
            windowListAdapter.setChildValues(allWindowsStates);
            windowListAdapter.notifyDataSetChanged();
        }
    };
    
    ExpandableListAdapter.ChildClickListener childClickListener = new ExpandableListAdapter.ChildClickListener() {
        @Override
        public void OnChildClick(View view, int groupPosition, int childPosition) {
            Log.d(TAG, "你切换了"+locations[groupPosition]+"中的"+ windows[groupPosition][childPosition]);

            AppCompatToggleButton clickedButton = (AppCompatToggleButton)view;
            boolean isChecked = clickedButton.isChecked();

            String command;
            Drawable drawable = ContextCompat.getDrawable(requireContext(), R.drawable.ic_round_sensor_window_24);

            if (groupPosition == 0) {
                if (childPosition == 0) {
                    command = "windows1:"+(isChecked?"1":"0");
                    boolean[] states = model.getBulbStates().getValue();

                    myOneNetApi.sendCommand(0, 0, command, response -> {
                        if (response.body() != null && response.body().get("errno").getAsInt() == 0) {
                            Toast.makeText(getContext(), "成功创建命令", Toast.LENGTH_SHORT).show();

                            //更新ViewModel中的数据
                            states[childPosition] = isChecked;
                            model.setWindowStates(states);
                        }
                        else if (response.body().get("errno").getAsInt() == 10) {
                            clickedButton.setChecked(!isChecked);
                            Toast.makeText(getContext(), "设备不在线", Toast.LENGTH_SHORT).show();
                        }
                        //可以控制的灯更新视图
                        if (clickedButton.isChecked()) {
                            drawable.setTint(getResources().getColor(R.color.white));
                            clickedButton.setTextColor(getResources().getColor(R.color.white));
                        }
                        else {
                            drawable.setTint(getResources().getColor(R.color.darker_gray));
                            clickedButton.setTextColor(getResources().getColor(R.color.darker_gray));
                        }
                        clickedButton.setCompoundDrawablesRelativeWithIntrinsicBounds(drawable,null,null,null);
                    });
                }
            }
            //所有灯更新视图
            if (clickedButton.isChecked()) {
                drawable.setTint(getResources().getColor(R.color.white));
                clickedButton.setTextColor(getResources().getColor(R.color.white));
            }
            else {
                drawable.setTint(getResources().getColor(R.color.darker_gray));
                clickedButton.setTextColor(getResources().getColor(R.color.darker_gray));
            }
            clickedButton.setCompoundDrawablesRelativeWithIntrinsicBounds(drawable,null,null,null);
        }
    };
}