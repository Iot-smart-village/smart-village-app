package com.example.smartvillage.cunmin;

import android.os.Bundle;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import androidx.appcompat.app.AppCompatActivity;

import com.example.smartvillage.R;

public class CallActivity extends AppCompatActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_call);

        WebView webView = new WebView(this);
        webView.setWebViewClient(new WebViewClient() {
            //设置在webView点击打开的新网页在当前界面显示,而不跳转到新的浏览器中
            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                view.loadUrl(url);
                return true;
            }
        });
        webView.getSettings().setJavaScriptEnabled(true);
        webView.getSettings().setDomStorageEnabled(true);
        webView.loadUrl("https://open.iot.10086.cn/onenetview/presentation.html#/?mode=share&tabno=0&s=b8e17bfd3c0d4dc9");          //调用loadUrl方法为WebView加入链接
        setContentView(webView);                           //调用Activity提供的setContentView将webView显示出来
    }

    // 定义WebChromeClient
//    private WebChromeClient mSearchChromeClient = new WebChromeClient() {
//        @Override
//        public void onProgressChanged(WebView view, int newProgress) {
//            Log.d("SEARCH_TAG", "on page progress changed and progress is " + newProgress);
//            // 进度是100就代表dom树加载完成了
//            if (newProgress == 100) {
//                //内嵌js脚本
//                webView.loadUrl("JavaScript:function mFunct(){$(\"div#logo\").next().next().next().next().eq(0).style.display='none';}mFunct();");
//            }
//
//        }
//    };
//        String URL = "https://open.iot.10086.cn/onenetview/presentation.html#/?atabno=0&mode=share&tabno=0&s=8882f06e61fcb1cc";
//        conUrl = "https://open.iot.10086.cn/onenetview/presentation.html#/?atabno=0&mode=share&tabno=0&s=8882f06e61fcb1cc"
//        URL httpUrl = new URL(conUrl);
//        HttpURLConnection httpconn = (HttpURLConnection) httpUrl.openConnection();
//        httpconn.setConnectTimeout(3000);
//        httpconn.setReadTimeout(5000);
//        int httpCode = httpconn.getResponseCode();

}