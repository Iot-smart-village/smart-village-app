package com.example.smartvillage.cunmin;

import androidx.appcompat.app.AppCompatActivity;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.SystemClock;
import android.view.View;
import android.widget.ImageView;

import com.example.smartvillage.R;
import com.google.android.material.button.MaterialButton;
import com.google.android.material.textfield.TextInputEditText;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.yzq.zxinglibrary.encode.CodeCreator;

public class InformationGatherActivity extends AppCompatActivity {

    TextInputEditText et_name,et_age;
    MaterialButton btn_create_code;
    ImageView qrCode;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_information_gather);

        btn_create_code = findViewById(R.id.info_gather_create_code);
        et_name = findViewById(R.id.info_gather_name);
        et_age = findViewById(R.id.info_gather_age);
        qrCode = findViewById(R.id.imageView3);

        btn_create_code.setOnClickListener(v -> {

            String contentEtString;
            JsonObject jsonObject = new JsonObject();
            if (et_name.getText() != null && et_age.getText() != null) {
                jsonObject.add("cunmin_name", JsonParser.parseString(et_name.getText().toString()));
                jsonObject.add("cunmin_age", JsonParser.parseString(et_age.getText().toString()));
                jsonObject.add("cunmin_time", JsonParser.parseString(String.valueOf(System.currentTimeMillis())));
            }
            contentEtString = jsonObject.toString();

            Bitmap bitmap = CodeCreator.createQRCode(contentEtString, 512, 512, null);
            BitmapDrawable bitmapDrawable = new BitmapDrawable(getResources(), bitmap);

            qrCode.setImageDrawable(bitmapDrawable);
        });
    }
}