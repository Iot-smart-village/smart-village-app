package com.example.smartvillage.cunmin;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.smartvillage.R;
import com.example.smartvillage.model.Commodity;

import cn.bmob.v3.exception.BmobException;
import cn.bmob.v3.listener.SaveListener;

public class DealEditActivity extends AppCompatActivity {
    TextView tx_tittle, tx_phone, tx_describe;
    Button button;
    EditText et_tittle, et_describe, et_phone;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_deal_edit);
        tx_tittle = findViewById(R.id.commodity_tittle);
        tx_phone = findViewById(R.id.commodity_phone);
        tx_describe = findViewById(R.id.commodity_describe);
        button = findViewById(R.id.post);
        et_describe = findViewById(R.id.tell_edit);
        et_phone = findViewById(R.id.phone_edit);
        et_tittle = findViewById(R.id.name_edit);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Commodity commodity = new Commodity();
                commodity.setTitle(et_tittle.getText().toString());
                commodity.setDescribe(et_describe.getText().toString());
                commodity.setPhone(et_phone.getText().toString());
                commodity.save(new SaveListener<String>() {
                    @Override
                    public void done(String s, BmobException e) {
                        if (e == null) {
                            Toast.makeText(DealEditActivity.this,"发布成功！",Toast.LENGTH_SHORT).show();
                        } else {
                            Toast.makeText(DealEditActivity.this,"发布失败！",Toast.LENGTH_SHORT).show();
                            Log.i("Bmob商品信息", "错误码："+e.getErrorCode()+"，错误描述："+e.getMessage());
                        }
                    }
                });

            }
        });
    }
}