package com.example.smartvillage;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.navigation.NavController;
import androidx.navigation.fragment.NavHostFragment;

import com.example.smartvillage.model.User;
import com.google.android.material.bottomnavigation.BottomNavigationView;

public class LaunchFragment extends Fragment {

    private static final String TAG = "启动页";

    MainViewModel model;
    SharedPreferences sharedPref;

    NavController navController;
    BottomNavigationView bottomNav;
    TextView tv_skip;

    public LaunchFragment() {
        super(R.layout.fragment_launch);
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requireActivity().getWindow().addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN | WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS);

        navController = NavHostFragment.findNavController(this);

        model = new ViewModelProvider(requireActivity()).get(MainViewModel.class);
        sharedPref = requireContext().getSharedPreferences(
                getString(R.string.preference_file_key), Context.MODE_PRIVATE);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_launch, container, false);

        //进入登陆界面时隐藏底部导航栏与应用栏
        bottomNav = requireActivity().findViewById(R.id.bottom_nav);
        bottomNav.setVisibility(View.GONE);
        ((AppCompatActivity) requireActivity()).getSupportActionBar().hide();

        tv_skip = view.findViewById(R.id.tv_skip);
        int millis_countdown = 5000;
        CountDownTimer countDownTimer = new CountDownTimer(millis_countdown, 1000) {
            int sec_countdown = millis_countdown/1000;
            @Override
            public void onTick(long millisUntilFinished) {
                tv_skip.setText(getString(R.string.skip, sec_countdown--));
            }

            @Override
            public void onFinish() {
                ifLoginAndNavigate();//倒计时结束后跳转
            }
        };
        countDownTimer.start();

        tv_skip.setOnClickListener(v -> {
            countDownTimer.cancel();//取消倒计时
            ifLoginAndNavigate();//并跳转
        });

        return view;
    }

    void ifLoginAndNavigate() {
        String str_username;
        int identity_num;
        //判断是否登录
        if((str_username = sharedPref.getString("current_user_name", null)) != null &&
                (identity_num = sharedPref.getInt("current_user_identity", 0)) != 0) {
            //已登录，保存用户信息到ViewModel
            User currentUser = model.getUser().getValue();
            currentUser.setUserName(str_username);
            currentUser.setIdentity(identity_num);
            model.setUser(currentUser);
            navController.navigate(R.id.action_launchFragment_to_homeFragment);//已登录，跳转至首页
        }
        else {
            navController.navigate(R.id.action_launchFragment_to_identityFragment);//未登录，跳转至登录
        }
    }
}