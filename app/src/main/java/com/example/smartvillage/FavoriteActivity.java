package com.example.smartvillage;

import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import java.util.ArrayList;
import java.util.HashMap;

public class FavoriteActivity extends AppCompatActivity {
    //1.数据（内容）
    String[] mydata = {
            "3000年前的三星堆，竟与世界有着这样的神奇“互动”",
            "名创优品被立案调查！店里包裹摄像头的胶带下的万店掌：顾客毫不知情，面部被打上各种标签",
            "苹果突然官宣：该产品全球下架！",
            "终于来了！中美会谈后 两国宣布重要合作 世界同时嗅到不寻常信号",
            "赔了夫人又折兵？拜登公开出洋相，特朗普儿子都看不下去了",
            "有什么样疾病的人不能打疫苗？国家卫健委回应",
            "唇枪舌剑之下的中美高层对话已经落幕，会谈究竟有哪些成果？",
            "医生教你8种健康的生活方式预防皮肤衰老，减少皮肤皱纹",
            "iPhone 12不送充电器，苹果被罚了！",};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_favorite);
        ListView listView= findViewById(R.id.lv);
        //2.组织数据
        ArrayList<HashMap<String, Object>> books = new ArrayList<HashMap<String, Object>>();
        for (int i = 0; i < mydata.length; i++) {
            HashMap<String, Object> book = new HashMap<String, Object>();
            book.put("tltle", mydata[i]);
            book.put("number", i+1);

            books.add(book);
        }

        //3.把数据和Item组合,即实现adapter
        SimpleAdapter myaddlistadapter = new SimpleAdapter(
                this,
                books,
                R.layout.item_favorite,
                new String[]{"number", "tltle"},
                new int[]{
                        R.id.item_number,
                        R.id.item_text});
        //4.显示Listview
        listView.setAdapter(myaddlistadapter);
        //5.操作listview
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Toast.makeText(
                        FavoriteActivity.this,
                        "你点的是第"+position+"行"+"    "+mydata[position],
                        Toast.LENGTH_SHORT).show();
            }
        });


    }
}