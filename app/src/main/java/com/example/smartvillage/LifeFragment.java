package com.example.smartvillage;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.smartvillage.adapters.LifeGridRecyclerAdapter;
import com.example.smartvillage.cunmin.AutoAdaptFragment;
import com.example.smartvillage.cunmin.BalconyFragment;
import com.example.smartvillage.cunmin.CallActivity;
import com.example.smartvillage.cunmin.DealActivity;
import com.example.smartvillage.cunmin.DoctorEntranceFragment;
import com.example.smartvillage.cunmin.DoorSecurityFragment;
import com.example.smartvillage.cunmin.InformationGatherActivity;
import com.example.smartvillage.cunmin.LeaveHomeFragment;
import com.example.smartvillage.cunmin.LightControlFragment;
import com.example.smartvillage.cunmin.LogisticsActivity;
import com.example.smartvillage.cunmin.WindowControlFragment;
import com.example.smartvillage.cunwei.InformationListActivity;
import com.example.smartvillage.cunwei.NoticeActivity;
import com.example.smartvillage.cunwei.RoadblockFragment;
import com.example.smartvillage.cunwei.StreetLampFragment;

public class LifeFragment extends Fragment implements LifeGridRecyclerAdapter.ItemClickListener {

    private static final String TAG = "生活";
    private MainViewModel model;

    String[] itemNameSet;
    int[] itemIconSet;

    public LifeFragment() {
        super(R.layout.fragment_life);
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        model = new ViewModelProvider(requireActivity()).get(MainViewModel.class);

        //根据不同的身份初始化不同的item的数据
        switch (model.getUser().getValue().getIdentity()) {
            case 1:
                itemNameSet = new String[]{"远程问诊", "一键呼叫", "远程监控", "门与安防", "快递提醒", "灯光控制", "窗户控制", "阳台控制", "离家模式", "自动调节", "交易中心", "信息收集"};
                itemIconSet = new int[]{
                          R.drawable.ic_round_local_hospital_24
                        , R.drawable.ic_round_call_24
                        , R.drawable.ic_round_camera_indoor_24
                        , R.drawable.ic_round_doorbell_24
                        , R.drawable.ic_round_delivery_dining_24
                        , R.drawable.ic_round_light_24
                        , R.drawable.ic_round_sensor_window_24
                        , R.drawable.ic_round_balcony_24
                        , R.drawable.ic_round_time_to_leave_24
                        , R.drawable.ic_round_dashboard_24
                        , R.drawable.ic_round_shopping_bag_24
                        , R.drawable.ic_round_wysiwyg_24};
                break;

            case 2:
                itemNameSet = new String[]{"路灯管理","发公告","陌生车辆","监控系统","路障系统","村民信息"};
                itemIconSet = new int[]{
                          R.drawable.ic_round_lightbulb_24
                        , R.drawable.ic_round_announcement_24
                        , R.drawable.ic_round_directions_car_24
                        , R.drawable.ic_round_monitor_24
                        , R.drawable.ic_round_do_not_disturb_on_24
                        , R.drawable.ic_round_wysiwyg_24};
                break;

            case 3:
                itemNameSet = new String[]{"呼叫显示","视频问诊","提醒看病"};
                itemIconSet = new int[]{
                        R.drawable.ic_round_phone_callback_24
                        , R.drawable.ic_round_video_call_24
                        , R.drawable.ic_round_notification_add_24};
                break;
        }
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        RecyclerView view =  (RecyclerView) inflater.inflate(R.layout.fragment_life, container, false);

        //列数为3
        GridLayoutManager gridLayoutManager = new GridLayoutManager(getContext(), 3);

        LifeGridRecyclerAdapter lifeGridRecyclerAdapter = new LifeGridRecyclerAdapter(itemNameSet, itemIconSet);
        //为Adapter设置点击监听器
        lifeGridRecyclerAdapter.setClickListener(this);

        view.setLayoutManager(gridLayoutManager);
        view.setAdapter(lifeGridRecyclerAdapter);
        view.setItemAnimator(new DefaultItemAnimator());

        return view;
    }

    @Override
    public void onItemClick(View view, int position) {
        //通过position判断不同的item被点击
        Log.d(TAG, "你点击了 位置：" + position);

        switch (model.getUser().getValue().getIdentity()) {
            case 1://村民
                switch (position) {//0远程问诊1一键呼叫2远程监控3门与安防4快递提醒5灯控制系统6窗户控制7阳台控制8离家模式9自动调节模式10交易中心11信息收集
                    case 0:
                        new DoctorEntranceFragment().show(getParentFragmentManager(),"DoctorEntrance");
                        break;
                    case 1:
                        Intent intent1 = new Intent(requireActivity(), CallActivity.class);
                        LifeFragment.super.requireContext().startActivity(intent1);
                        break;
                    case 2:
                        //Intent intent2 = new Intent(getActivity(), MonitorActivity.class);
                        //LifeFragment.super.getContext().startActivity(intent2);
                        Uri uri = Uri.parse("http://192.168.1.100:81/stream");
                        Intent intent = new Intent(Intent.ACTION_VIEW, uri);
                        startActivity(intent);
                        break;
                    case 3:
                        new DoorSecurityFragment().show(getParentFragmentManager(), "DoorSecurity");
                        break;
                    case 4:
                        Intent intent4 = new Intent(requireActivity(), LogisticsActivity.class);
                        LifeFragment.super.requireContext().startActivity(intent4);
                        break;
                    case 5:
                        new LightControlFragment().show(getParentFragmentManager(),"LightControl");
                        break;
                    case 6:
                        new WindowControlFragment().show(getParentFragmentManager(),"WindowControl");
                        break;
                    case 7:
                        new BalconyFragment().show(getParentFragmentManager(), "BalconyControl");
                        break;
                    case 8:
                        new LeaveHomeFragment().show(getParentFragmentManager(), "LeaveHomeControl");
                        break;
                    case 9:
                        new AutoAdaptFragment().show(getParentFragmentManager(),"AutoAdapt");
                        break;
                    case 10:
                        Intent intent10 = new Intent(requireActivity(), DealActivity.class);
                        LifeFragment.super.requireContext().startActivity(intent10);
                        break;
                    case 11:
                        Intent intent11 = new Intent(requireActivity(), InformationGatherActivity.class);
                        LifeFragment.super.requireContext().startActivity(intent11);
                        break;
                    case 12:
                        break;
                }
                break;
            case 2:
                switch (position) {//0路灯管理1发广播2发公告3陌生车辆识别记录4监控系统管理5路障系统管理
                    case 0:
                        new StreetLampFragment().show(getParentFragmentManager(),"StreetLamp");
                        break;
                    case 1:
                        Intent intent21 = new Intent(requireActivity(), NoticeActivity.class);
                        LifeFragment.super.requireContext().startActivity(intent21);
                        break;
                    case 2:
                        break;
                    case 3:
                        break;
                    case 4:
                        new RoadblockFragment().show(getParentFragmentManager(),"Roadblock");
                        break;
                    case 5:
                        Intent intent5 = new Intent(requireActivity(), InformationListActivity.class);
                        LifeFragment.super.requireContext().startActivity(intent5);
                        break;
                }
                break;
            case 3:
                switch (position) {//0呼叫显示1远程视频问诊2提醒看病
                    case 0:
                        break;
                    case 1:
                        break;
                    case 2:
                        break;
                }
                break;
        }
    }
}